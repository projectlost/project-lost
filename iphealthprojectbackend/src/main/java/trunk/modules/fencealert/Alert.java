package trunk.modules.fencealert;

public class Alert {
	
	Double latitude;
	Double longitude;
	
	public Alert() {
		
	}
	
	public Alert(Double lat, Double lng) {
		this.latitude = lat;
		this.longitude = lng;
	}
	
	public Double getLat() {
		return this.latitude;
	}
	
	public void setLat(Double lat) {
		this.latitude = lat;
	}
	
	public Double getLng() {
		return this.longitude;
	}
	
	public void setLng(Double lng) {
		this.longitude = lng;
	}
	

}
