package trunk.modules.fencealert;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.SingleSideMenu;
import trunk.base.module.ui.sideMenu.file.TrunkFile;

/*
 * Every module should implement base interface (IMathsController) in this case and extend TrunkModuleBase.
 * 
 * @Controller annotation has to be present as well
 */
@Controller
public class FenceAlertController extends TrunkModuleBase implements IFenceAlertController {

	/* 
	 * Creates a object for Alert class
	 */
	@Override
	public Alert alertFence(Double lat, Double lng, HttpServletRequest req, HttpServletResponse res) {
		Alert trigAlert = new Alert(lat, lng);

		/*
		 * Return the result. This gets serialized to json
		 */
		return trigAlert;
	}

	/*
	 * Add side menu items for this module. Overridden method from
	 * TrunkModuleBase
	 */
	@Override
	public void initializeUI(ModuleUI ui) {

		/*
		 * NOTE: All file URLS are relative to webapp/modules/<module id>
		 * directory
		 */
		
		ui.addCssFile(new TrunkFile(ui, "/fencealert.css"));
		ui.addJsFile(new TrunkFile(ui, "/fencealert.js"));

		SingleSideMenu sm = new SingleSideMenu(ui);

		/*
		 * URL of the page. This is the URL after http://localhost:8989/#/
		 */
		sm.setHref("/");
		sm.setName("Alerting");
		sm.setIcon("fa fa-map-marker");
		/*
		 * View to load then this menu item is clicked
		 */
		sm.setTemplate("/fencealert.html");
		sm.setId("alertMainSideSingle");

		/*
		 * Attach the side menu to UI
		 */
		ui.addSideMenu(sm);
	}
}