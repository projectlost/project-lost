package trunk.modules.navigation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.annotations.TrunkWorkflowFeature;
import trunk.base.module.ITrunkModuleBase;

/*
 * Base URL for module.
 *
 * In this case the URL would be http://localhost:8080/maths/<feature id> 
 */
@RequestMapping("/navigation")
/*
 * id: directly maps to module folder name under webapp/modules/<module id>
 * 
 * module id must be unique
 */
@TrunkModuleFeature(id = "navigation", displayName = "Navigation", addToDB = true, forSuperAdmin = false)
/*
 * All module base interfaces must extend ITrunkModuleBase
 */
public interface INavigationController extends ITrunkModuleBase {

	@TrunkWorkflowFeature(
			/*
			 * View associated with this feature
			 */
			view = "/workflow/navigationWorkflow.html",
			/*
			 * CSS files to load during feature preview
			 */
			cssFiles = {
					"/navigation.css",
			},
			/*
			 * Javascript files to be loaded before initializing the view
			 */
			jsFiles = {
					"/navigation.js",
			},
			/*
			 * CSS files to load when settings dialog is loaded
			 */
			settingsCssFiles = {
					"/navigation.css",
			},
			/*
			 * View for settings dialog
			 */
			settings = "/workflow/settings.html",

			/*
			 * The Javascript files to load before the settings dialog is
			 * displayed
			 */
			settingsJsFiles = { "/workflow/settings.js"
			})
	/*
	 * id: feature id. Must be the same as RequestMapping value and must be
	 * unique
	 */
	@TrunkModuleFeature(id = "showNavigation", displayName = "Navigation")
	/*
	 * Must be the same as feature id.
	 * 
	 * Second part of the web service URL
	 * 
	 * In this case, the URL would be http://localhost:8080/maths/add
	 */
	@RequestMapping(value = "/showNavigation", method = RequestMethod.POST)
	@ResponseBody
	Navigate showNavigation(

			/*
			 * RequestParam value name is the name of the parameter in the json
			 * object parameter passed in while making the request
			 * 
			 * 
			 */
			@RequestParam(value = "location", required = true) String location,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);

	
}