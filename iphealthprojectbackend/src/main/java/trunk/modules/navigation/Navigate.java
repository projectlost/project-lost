package trunk.modules.navigation;

public class Navigate {
	
	String location;
	
	public Navigate() {
		
	}
	
	public Navigate(String loc) {
		this.location = loc;
	}
	
	public String getLocation() {
		return this.location;
	}
	
	public void setLocation(String loc) {
		this.location = loc;
	}
	

}
