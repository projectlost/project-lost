package trunk.modules.geofencing;

import java.sql.Types;
import java.util.List;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;

/*
 * The database access object.
 * 
 * Must have the @Repository annotation and must extend JdbcDaoSupportBase
 * 
 * All database calls MUST be done using stored procedures.
 */
@Repository
public class GeoFenceDao extends JdbcDaoSupportBase {

	/*
	 * Inserts the fence value into the database using the procedure
	 */
	public GeoFence saveFence(String name, String location, Double radius, int uid, Double lat, Double lng) {

		return invokeStoredProc(GeoFence.class, "x_save_a_fence",
				new StoredProcParam(Types.VARCHAR, "name", name),
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "loc", location),
				new StoredProcParam(Types.DOUBLE, "rad", radius),
				new StoredProcParam(Types.DOUBLE, "lat", lat),
				new StoredProcParam(Types.DOUBLE, "lng", lng));
	}
	
	/*
	 * Retrieves a list of values from the database with the use of a procedure
	 */
	public List<GeoFence> listFences(int uid) {

		return invokeStoredProcMulti(GeoFence.class, "x_get_all_fences",
				new StoredProcParam(Types.INTEGER, "uid", uid));
	}
}