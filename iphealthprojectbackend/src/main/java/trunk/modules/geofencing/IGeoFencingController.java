package trunk.modules.geofencing;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.annotations.TrunkWorkflowFeature;
import trunk.base.module.ITrunkModuleBase;

/*
 * Base URL for module.
 *
 * In this case the URL would be http://localhost:8080/maths/<feature id> 
 */
@RequestMapping("/geofencing")
/*
 * id: directly maps to module folder name under webapp/modules/<module id>
 * 
 * module id must be unique
 */
@TrunkModuleFeature(id = "geofencing", displayName = "Geofencing", addToDB = true, forSuperAdmin = false)
/*
 * All module base interfaces must extend ITrunkModuleBase
 */
public interface IGeoFencingController extends ITrunkModuleBase {

	@TrunkWorkflowFeature(
			/*
			 * View associated with this feature
			 */
			view = "/workflow/geoFencingWorkflow.html",
			/*
			 * CSS files to load during feature preview
			 */
			cssFiles = {
					"/geofencing.css",
			},
			/*
			 * Javascript files to be loaded before initializing the view
			 */
			jsFiles = {
					"/geofencing.js",
			},
			/*
			 * CSS files to load when settings dialog is loaded
			 */
			settingsCssFiles = {
					"/geofencing.css",
			},
			/*
			 * View for settings dialog
			 */
			settings = "/workflow/settings.html",

			/*
			 * The Javascript files to load before the settings dialog is
			 * displayed
			 */
			settingsJsFiles = { "/workflow/settings.js"
			})
	/*
	 * id: feature id. Must be the same as RequestMapping value and must be
	 * unique
	 */
	@TrunkModuleFeature(id = "drawFence", displayName = "Geofence")
	/*
	 * Must be the same as feature id.
	 * 
	 * Second part of the web service URL
	 * 
	 * In this case, the URL would be http://localhost:8080/maths/add
	 */
	@RequestMapping(value = "/drawFence", method = RequestMethod.POST)
	@ResponseBody
	GeoFence drawFence(

			/*
			 * RequestParam value name is the name of the parameter in the json
			 * object parameter passed in while making the request
			 * 
			 * Example: var parameter = {'num1': 1, 'num2': 2}
			 */
			@RequestParam(value = "Name", required = true) String name,
			@RequestParam(value = "Location", required = true) String location,
			@RequestParam(value = "Radius", required = true) Double radius,
			@RequestParam(value = "Lat", required = true) Double lat,
			@RequestParam(value = "Lng", required = true) Double lng,

			/*
			 * HttpServletRequest and HttpServletResponse gets injected
			 * automatically
			 */
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
	
			@TrunkModuleFeature(id = "listFences", displayName = "List all fences")
			@RequestMapping(value = "/listFences", method = RequestMethod.POST)
			@ResponseBody
			List<GeoFence> listFences(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	
}