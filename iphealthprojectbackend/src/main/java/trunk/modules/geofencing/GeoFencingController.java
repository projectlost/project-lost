package trunk.modules.geofencing;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.SingleSideMenu;
import trunk.base.module.ui.sideMenu.file.TrunkFile;
import trunk.interceptors.ApplicationContextProvider;

/*
 * Every module should implement base interface (IMathsController) in this case and extend TrunkModuleBase.
 * 
 * @Controller annotation has to be present as well
 */
@Controller
public class GeoFencingController extends TrunkModuleBase implements IGeoFencingController {

	/*
	 * Passes the values from the front end to the Data Access object 
	 */
	@Override
	public GeoFence drawFence(String name, String location, Double radius, Double lat, Double lng, HttpServletRequest req, HttpServletResponse res) {

		User u = SessionProvider.getUser(req);
		
		GeoFenceDao gfDao = ApplicationContextProvider.getContext().getBean(GeoFenceDao.class);
		
		return gfDao.saveFence(name, location, radius, u.getId(), lat, lng);
	}
	
	/*
	 * Returns a list of fence values from the database
	 */
	@Override
	public List<GeoFence> listFences(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);
		GeoFenceDao gfDao = ApplicationContextProvider.getContext().getBean(GeoFenceDao.class);

		return gfDao.listFences(u.getId());
	}
	
	
	/*
	 * Add side menu items for this module. Overridden method from
	 * TrunkModuleBase
	 */
	@Override
	public void initializeUI(ModuleUI ui) {

		/*
		 * NOTE: All file URLS are relative to webapp/modules/<module id>
		 * directory
		 */
		
		ui.addCssFile(new TrunkFile(ui, "/geofencing.css"));
		ui.addJsFile(new TrunkFile(ui, "/geofencing.js"));

		SingleSideMenu sm = new SingleSideMenu(ui);

		/*
		 * URL of the page. This is the URL after http://localhost:8989/#/
		 */
		sm.setHref("/");
		sm.setName("Geofencing");
		sm.setIcon("fa fa-map-marker");
		/*
		 * View to load then this menu item is clicked
		 */
		sm.setTemplate("/geofencing.html");
		sm.setId("fenceMainSideSingle");

		/*
		 * Attach the side menu to UI
		 */
		ui.addSideMenu(sm);
	}
}