package trunk.modules.geofencing;

public class GeoFence {
	
	private int id;
	private int user_id;
	private String location;
	private Double radius;
	private String name;
	private Double lat;
	private Double lng;
	
	public GeoFence()
	{
		
	}
	
	public GeoFence(String name, String location, Double radius, Double lat, Double lng)
	{
		this.name = name;
		this.location = location;
		this.radius = radius;
		this.lat = lat;
		this.lng = lng;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	public String getLocation()
	{
		return this.location;
	}
	
	public void setLocation(String location)
	{
		this.location = location;
	}
	
	public Double getRadius()
	{
		return this.radius;
	}
	
	public void setRadius(Double rad)
	{
		this.radius = rad;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public Double getLat()
	{
		return this.lat;
	}
	
	public void setLat(Double lat)
	{
		this.lat = lat;
	}
	
	public Double getLng()
	{
		return this.lng;
	}
	
	public void setLng(Double lng)
	{
		this.lng = lng;
	}

}
