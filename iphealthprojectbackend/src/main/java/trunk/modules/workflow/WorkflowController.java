package trunk.modules.workflow;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.SingleSideMenu;
import trunk.base.module.ui.sideMenu.file.TrunkFile;
import trunk.base.store.ModuleStore;
import trunk.base.store.TrunkFeature;

@Controller
public class WorkflowController extends TrunkModuleBase implements IWorkflowController {

	@Override
	public TrunkFeature getActionSettings(String mid, String fid, HttpServletRequest req, HttpServletResponse res) {
		TrunkFeature afb = ModuleStore.getWorkflowFeatureFromModuleAndFeatureIds(mid, fid);

		if (afb == null) {
			if (res != null) {
				returnError(res, "Requested feature is not present");
			}

			return null;
		}

		return afb;
	}

	@Override
	public void initializeUI(ModuleUI ui) {
		ui.addJsFile(new TrunkFile(ui, "/test/testworkflowfeature.js"));

		SingleSideMenu sm = new SingleSideMenu(ui);
		sm.setHref("/testfeature");
		sm.setName("Test Workflow Feature");
		sm.setIcon("fa fa-road");
		sm.setTemplate("/test/testworkflowfeature.html");

		ui.addSideMenu(sm);
	}
}