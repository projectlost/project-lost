package trunk.modules.multiplelocations;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.annotations.TrunkWorkflowFeature;
import trunk.base.module.ITrunkModuleBase;

/*
 * Base URL for module.
 *
 * In this case the URL would be http://localhost:8080/maths/<feature id> 
 */
@RequestMapping("/multiplelocations")
/*
 * id: directly maps to module folder name under webapp/modules/<module id>
 * 
 * module id must be unique
 */
@TrunkModuleFeature(id = "multiplelocations", displayName = "Multiple locations", addToDB = true, forSuperAdmin = false)
/*
 * All module base interfaces must extend ITrunkModuleBase
 */
public interface IMultipleLocationsController extends ITrunkModuleBase {

	@TrunkWorkflowFeature(
			/*
			 * View associated with this feature
			 */
			view = "/workflow/multiplelocationsWorkflow.html",
			/*
			 * CSS files to load during feature preview
			 */
			cssFiles = {
					"/multiplelocations.css",
			},
			/*
			 * Javascript files to be loaded before initializing the view
			 */
			jsFiles = {
					"/multiplelocations.js",
			},
			/*
			 * CSS files to load when settings dialog is loaded
			 */
			settingsCssFiles = {
					"/multiplelocations.css",
			},
			/*
			 * View for settings dialog
			 */
			settings = "/workflow/settings.html",

			/*
			 * The Javascript files to load before the settings dialog is
			 * displayed
			 */
			settingsJsFiles = { "/workflow/settings.js"
			})
	/*
	 * id: feature id. Must be the same as RequestMapping value and must be
	 * unique
	 */
	@TrunkModuleFeature(id = "saveAddress", displayName = "Address")
	@RequestMapping(value = "/saveAddress", method = RequestMethod.POST)
	@ResponseBody
	MultipleLocations saveAddress(

			/*
			 * RequestParam value name is the name of the parameter in the json
			 * object parameter passed in while making the request
			 * 
			 * 
			 */
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "user", required = true) String user,
			@RequestParam(value = "grp", required = true) String grp,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
	
	@TrunkModuleFeature(id = "listUserLocation", displayName = "Address")
	@RequestMapping(value = "/listUserLocation", method = RequestMethod.POST)
	@ResponseBody
	List<MultipleLocations> listUserLocation(
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "listAddDetails", displayName = "Address")
	@RequestMapping(value = "/listAddDetails", method = RequestMethod.POST)
	@ResponseBody
	List<MultipleLocations> listAddDetails(
			@RequestParam(value = "grp", required = true) String grp,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	
}