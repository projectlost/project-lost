package trunk.modules.multiplelocations;

import java.sql.Types;
import java.util.List;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;

/*
 * The database access object.
 * 
 * Must have the @Repository annotation and must extend JdbcDaoSupportBase
 * 
 * All database calls MUST be done using stored procedures.
 */
@Repository
public class MultipleLocationsDao extends JdbcDaoSupportBase {

	/*
	 * Inserts data into address_details table
	 */
	public MultipleLocations saveUserAddress(int uid, String address, String user, String grp) {

		return invokeStoredProc(MultipleLocations.class, "x_save_multiple_location",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "address", address),
				new StoredProcParam(Types.VARCHAR, "user_name", user),
				new StoredProcParam(Types.VARCHAR, "grp_no", grp));
	}
	
	/*
	 * Retrieves data from the address_details table based on the user name
	 */
	public List<MultipleLocations> listUserAddress(String user_name) {

		return invokeStoredProcMulti(MultipleLocations.class, "x_get_multiple_location",
				new StoredProcParam(Types.VARCHAR, "u_name", user_name));
	}
	
	/*
	 * Retrieves data from the address_details table based on the user name and group
	 */
	public List<MultipleLocations> listGrp(String user_name, String grp) {

		return invokeStoredProcMulti(MultipleLocations.class, "x_get_group_details",
				new StoredProcParam(Types.VARCHAR, "u_name", user_name),
				new StoredProcParam(Types.VARCHAR, "grp_no", grp));
	}
	

}