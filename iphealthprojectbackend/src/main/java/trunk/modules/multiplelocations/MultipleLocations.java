package trunk.modules.multiplelocations;

public class MultipleLocations {
	
	private int user_id;
	String address;
	String user;
	String group_no;
	
	public MultipleLocations() {
		
	}
	
	public MultipleLocations(String add,String user) {
		this.address = add;
		this.user = user;
	}
	
	public int getUserId() {
		return this.user_id;
	}
	
	public void setUserId(int uid) {
		this.user_id = uid;
	}
	
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String add) {
		this.address = add;
	}
	
	public String getUser() {
		return this.user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getGroup() {
		return this.group_no;
	}
	
	public void setGroup(String grp) {
		this.group_no = grp;
	}

}
