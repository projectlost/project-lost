package trunk.modules.multiplelocations;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.TrunkFile;
import trunk.interceptors.ApplicationContextProvider;

/*
 * Every module should implement base interface (IMathsController) in this case and extend TrunkModuleBase.
 * 
 * @Controller annotation has to be present as well
 */
@Controller
public class MultipleLocationsController extends TrunkModuleBase implements IMultipleLocationsController {

	@Override
	public MultipleLocations saveAddress(String address, String user,String grp, HttpServletRequest req, HttpServletResponse res) {
		
		User u = SessionProvider.getUser(req);
		MultipleLocationsDao mlDao = ApplicationContextProvider.getContext().getBean(MultipleLocationsDao.class);
		
		return mlDao.saveUserAddress(u.getId(), address, user, grp);
	}
	
	@Override
	public List<MultipleLocations> listUserLocation(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);
		MultipleLocationsDao mlDao = ApplicationContextProvider.getContext().getBean(MultipleLocationsDao.class);

		return mlDao.listUserAddress(u.getName());
	}
	
	@Override
	public List<MultipleLocations> listAddDetails(String grp,HttpServletRequest req) {
		User u = SessionProvider.getUser(req);
		MultipleLocationsDao mlDao = ApplicationContextProvider.getContext().getBean(MultipleLocationsDao.class);

		return mlDao.listGrp(u.getName(),grp);
	}

	/*
	 * Add side menu items for this module. Overridden method from
	 * TrunkModuleBase
	 */
	@Override
	public void initializeUI(ModuleUI ui) {

		/*
		 * NOTE: All file URLS are relative to webapp/modules/<module id>
		 * directory
		 */
		
		ui.addCssFile(new TrunkFile(ui, "/multiplelocations.css"));
		ui.addJsFile(new TrunkFile(ui, "/multiplelocations.js"));

		/*SingleSideMenu sm = new SingleSideMenu(ui);

		sm.setHref("/");
		sm.setName("Multiple Navigation");
		sm.setIcon("fa fa-map-marker");

		sm.setTemplate("/multiplelocations.html");
		sm.setId("mapMainSideSingle");

		ui.addSideMenu(sm);*/
		
		
		/* Multiside Menu */
		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-map-marker");
		msm.setName("Navigation Features");
		msm.setId("multiLocMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Multiple Navigation");
		ssm.setHref("/multiplelocations");
		ssm.setTemplate("/multiplelocations.html");
		ssm.setId("multiLocMainSideMulti1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Provide Locations");
		ssm2.setHref("/providelocations");
		ssm2.setTemplate("/providelocations.html");
		ssm2.setId("multiLocMainSideMulti2");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
	}
}