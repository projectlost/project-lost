package trunk.modules.messaging.beans;

public class MessageBean {

	/*
	 * The variable names and variable types must match with the name of the
	 * column returned as a result of the sql query executed.
	 * 
	 * Example:
	 * 
	 * VARCHAR maps to String
	 * 
	 * INTEGER maps to int
	 */
	private int id;
	private int user_id;
	private String message;

	/*
	 * Default empty constructor must be present for database to java object
	 * mapping to work
	 */
	public MessageBean() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
