package trunk.modules.messaging.dao;

import java.sql.Types;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;
import trunk.modules.messaging.beans.MessageBean;

/*
 * The database access object.
 * 
 * Must have the @Repository annotation and must extend JdbcDaoSupportBase
 * 
 * All database calls MUST be done using stored procedures.
 */
@Repository
@Transactional(rollbackFor = { Exception.class })
public class MessageDao extends JdbcDaoSupportBase {

	/*
	 * Also see MessageBean implementation
	 */
	public MessageBean sendMessage(String message, int uid) {

		/*
		 * invokeStoredProc invokes a stored procedure ('x_send_a_message' in
		 * this case) and serializes the returned result to a Java object
		 * ('MessageBean' in this case)
		 */
		return invokeStoredProc(MessageBean.class, "x_send_a_message",
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "mess", message));
	}

	public List<MessageBean> listMessages(int uid) {

		/*
		 * invokeStoredProcMulti invokes a stored procedure
		 * ('x_get_all_messages' in this case) and serializes the returned
		 * resultS (multiple rows) to a list of Java objects ('MessageBean' in
		 * this case)
		 */
		return invokeStoredProcMulti(MessageBean.class, "x_get_all_messages",
				new StoredProcParam(Types.INTEGER, "uid", uid));
	}

	public void DeleteMessage(int msgID) {
		invokeStoredProc("x_delete_a_message", new StoredProcParam(
				Types.INTEGER, "msg_id", msgID));
	}
}