package trunk.modules.masterschedule;


public class MasterSchedule {
	
	int id;
	String name;
	String unit;
	String street;
	String suburb;
	String city;
	String state;
	String post;
	int uid;
	String date;
	String assigned_user;
	Double lat;
	Double lng;
	
	public MasterSchedule() {
		
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getUid() {
		return this.uid;
	}
	
	public void setUid(int uid) {
		this.uid = uid;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUnit() {
		return this.unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getStreet() {
		return this.street;
	}
	
	public void setStreet(String st) {
		this.street = st;
	}
	
	public String getSubUrb() {
		return this.suburb;
	}
	
	public void setSubUrb(String sb) {
		this.suburb = sb;
	}
	
	public String getCity() {
		return this.city;
	}
	
	public void setCity(String ct) {
		this.city = ct;
	}
	
	public String getState() {
		return this.state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getPost() {
		return this.post;
	}
	
	public void setPost(String post) {
		this.post = post;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public void setDate(String dt) {
		this.date = dt;
	}
	
	public String getUsr() {
		return this.assigned_user;
	}
	
	public void setUsr(String usr) {
		this.assigned_user = usr;
	}
	
	public Double getLat() {
		return this.lat;
	}
	
	public void setLat(Double lt) {
		this.lat = lt;
	}
	
	public Double getLng() {
		return this.lng;
	}
	
	public void setLng(Double lg) {
		this.lng = lg;
	}
}
