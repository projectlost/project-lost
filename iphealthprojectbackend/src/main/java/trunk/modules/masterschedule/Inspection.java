package trunk.modules.masterschedule;

public class Inspection {
	
	int id;
	int pid;
	String date;
	int uid;
	String status;
	
	public Inspection() {
		
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getUid() {
		return this.uid;
	}
	
	public void setUid(int uid) {
		this.uid = uid;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public void setDate(String dt) {
		this.date = dt;
	}
	
	public int getPid() {
		return this.pid;
	}
	
	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public void setStatus(String st) {
		this.status = st;
	}

}
