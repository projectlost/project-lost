package trunk.modules.masterschedule;

public class Limits {
	
	int id;
	int user_id;
	String user_name;
	String date;
	int limits;
	
	public Limits() {
		
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getUid() {
		return this.user_id;
	}
	
	public void setUid(int uid) {
		this.user_id = uid;
	}
	
	public String getUname() {
		return this.user_name;
	}
	
	public void setUname(String uname) {
		this.user_name = uname;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public int getLimts() {
		return this.limits;
	}
	
	public void setLimits(int lmt) {
		this.limits = lmt;
	}

}
