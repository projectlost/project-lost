package trunk.modules.masterschedule;

import java.sql.Types;
import java.util.List;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;

/*
 * The database access object.
 * 
 * Must have the @Repository annotation and must extend JdbcDaoSupportBase
 * 
 * All database calls MUST be done using stored procedures.
 */
@Repository
public class MasterScheduleDao extends JdbcDaoSupportBase {

	/*
	 * Inserts value into the property table
	 */
	public MasterSchedule saveProp(String name, String unit, String street, String suburb, String city, String state, String postcode, int uid, String date, String usr, Double lat, Double lng) {

		return invokeStoredProc(MasterSchedule.class, "x_save_a_property",
				new StoredProcParam(Types.VARCHAR, "name", name),
				new StoredProcParam(Types.VARCHAR, "unit", unit),
				new StoredProcParam(Types.VARCHAR, "street", street),
				new StoredProcParam(Types.VARCHAR, "suburb", suburb),
				new StoredProcParam(Types.VARCHAR, "city", city),
				new StoredProcParam(Types.VARCHAR, "state", state),
				new StoredProcParam(Types.VARCHAR, "postcode", postcode),
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.VARCHAR, "dt", date),
				new StoredProcParam(Types.VARCHAR, "a_user", usr),
				new StoredProcParam(Types.DOUBLE, "lt", lat),
				new StoredProcParam(Types.DOUBLE, "lg", lng));
	}
	
	/*
	 * Retrieves value from the property table
	 */
	public List<MasterSchedule> listProp() {

		return invokeStoredProcMulti(MasterSchedule.class, "x_get_all_property");
	}
	
	/*
	 * Inserts data into inspection_details table
	 */
	public Inspection saveIns(int pid, String date, int uid) {

		return invokeStoredProc(Inspection.class, "x_save_inspection",
				new StoredProcParam(Types.INTEGER, "pid", pid),
				new StoredProcParam(Types.VARCHAR, "dt", date),
				new StoredProcParam(Types.INTEGER, "uid", uid));
	}
	
	/*
	 * Retrieves value from the inspection_details table based on the input parameters
	 */
	public List<MasterSchedule> retIns(String date, String usr) {

		return invokeStoredProcMulti(MasterSchedule.class, "x_get_ins_details",
				new StoredProcParam(Types.VARCHAR, "dt", date),
				new StoredProcParam(Types.VARCHAR, "usr", usr));
	}
	
	/*
	 * Updates values from the inspection_details table based on the id value
	 */
	public Inspection updIns(int id) {

		return invokeStoredProc(Inspection.class, "x_upd_inspection",
				new StoredProcParam(Types.INTEGER, "i_id", id));
	}
	
	/*
	 * Retrieves all the values from the inspection_details table
	 */
	public List<Inspection> listIns(int id) {

		return invokeStoredProcMulti(Inspection.class, "x_list_all_inspection",
				new StoredProcParam(Types.INTEGER, "i_id", id));
	}
	
	/*
	 * Retrieves value from the inspection_details table based on the 
	 * input parameters and status of the property
	 */
	public List<MasterSchedule> retInsStat(String date, String usr) {

		return invokeStoredProcMulti(MasterSchedule.class, "x_get_ins_details_status",
				new StoredProcParam(Types.VARCHAR, "dt", date),
				new StoredProcParam(Types.VARCHAR, "usr", usr));
	}
	
	/*
	 * Inserts the values to visit_limits table
	 */
	public Limits saveLmt(int uid, String usr_name,String date, int limit) {

		return invokeStoredProc(Limits.class, "x_save_limits",
				new StoredProcParam(Types.INTEGER, "u_id", uid),
				new StoredProcParam(Types.VARCHAR, "u_name", usr_name),
				new StoredProcParam(Types.VARCHAR, "dt", date),
				new StoredProcParam(Types.INTEGER, "lmt", limit));
	}
	
	/*
	 * Retrieves data from the visit_limits table
	 */
	public List<Limits> retLmt(String date, String usr) {

		return invokeStoredProcMulti(Limits.class, "x_ret_limits",
				new StoredProcParam(Types.VARCHAR, "dt", date),
				new StoredProcParam(Types.VARCHAR, "usr", usr));
	}
	
}