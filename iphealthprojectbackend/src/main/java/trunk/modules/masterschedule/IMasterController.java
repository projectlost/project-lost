package trunk.modules.masterschedule;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.annotations.TrunkWorkflowFeature;
import trunk.base.module.ITrunkModuleBase;

/*
 * Base URL for module.
 *
 * In this case the URL would be http://localhost:8080/maths/<feature id> 
 */
@RequestMapping("/masterschedule")
/*
 * id: directly maps to module folder name under webapp/modules/<module id>
 * 
 * module id must be unique
 */
@TrunkModuleFeature(id = "masterschedule", displayName = "Master Schedule", addToDB = true, forSuperAdmin = false)
/*
 * All module base interfaces must extend ITrunkModuleBase
 */
public interface IMasterController extends ITrunkModuleBase {

	@TrunkWorkflowFeature(
			/*
			 * View associated with this feature
			 */
			view = "/workflow/masterScheduleWorkflow.html",
			/*
			 * CSS files to load during feature preview
			 */
			cssFiles = {
					"/masterschedule.css",
			},
			/*
			 * Javascript files to be loaded before initializing the view
			 */
			jsFiles = {
					"/masterschedule.js",
			},
			/*
			 * CSS files to load when settings dialog is loaded
			 */
			settingsCssFiles = {
					"/masterschedule.css",
			},
			/*
			 * View for settings dialog
			 */
			settings = "/workflow/settings.html",

			/*
			 * The Javascript files to load before the settings dialog is
			 * displayed
			 */
			settingsJsFiles = { "/workflow/settings.js"
			})
	/*
	 * id: feature id. Must be the same as RequestMapping value and must be
	 * unique
	 */
	@TrunkModuleFeature(id = "saveProperty", displayName = "Save Property")
	@RequestMapping(value = "/saveProperty", method = RequestMethod.POST)
	@ResponseBody
	MasterSchedule saveProperty(

			@RequestParam(value = "Name", required = true) String name,
			@RequestParam(value = "Unit", required = true) String unit,
			@RequestParam(value = "Street", required = true) String street,
			@RequestParam(value = "Suburb", required = true) String suburb,
			@RequestParam(value = "City", required = true) String city,
			@RequestParam(value = "State", required = true) String state,
			@RequestParam(value = "Postcode", required = true) String postcode,
			@RequestParam(value = "Date", required = true) String date,
			@RequestParam(value = "User", required = true) String usr,
			@RequestParam(value = "Lat", required = true) Double lat,
			@RequestParam(value = "Lng", required = true) Double lng,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
	
	@TrunkModuleFeature(id = "listProperty", displayName = "List all properties")
	@RequestMapping(value = "/listProperty", method = RequestMethod.POST)
	@ResponseBody
	List<MasterSchedule> listProperty(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	
	@TrunkModuleFeature(id = "saveInspection", displayName = "Save Inspection")
	@RequestMapping(value = "/saveInspection", method = RequestMethod.POST)
	@ResponseBody
	Inspection saveInspection(

			@RequestParam(value = "Property Id", required = true) int pid,
			@RequestParam(value = "Date", required = true) String date,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
	
	@TrunkModuleFeature(id = "retInspection", displayName = "List inspection details")
	@RequestMapping(value = "/retInspection", method = RequestMethod.POST)
	@ResponseBody
	List<MasterSchedule> retInspection(
			@RequestParam(value = "date", required = true) String date,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "updInspection", displayName = "Save Inspection")
	@RequestMapping(value = "/updInspection", method = RequestMethod.POST)
	@ResponseBody
	Inspection updInspection(
			@RequestParam(value = "Id", required = true) int id);
	
	@TrunkModuleFeature(id = "listAllIns", displayName = "Save Inspection")
	@RequestMapping(value = "/listAllIns", method = RequestMethod.POST)
	@ResponseBody
	List<Inspection> listAllIns(
			@RequestParam(value = "Id", required = true) int id);
	
	@TrunkModuleFeature(id = "retInspectionStat", displayName = "List inspection details")
	@RequestMapping(value = "/retInspectionStat", method = RequestMethod.POST)
	@ResponseBody
	List<MasterSchedule> retInspectionStat(
			@RequestParam(value = "date", required = true) String date,
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "loadFromFile", displayName = "Load from file")
	@RequestMapping(value = "/loadFromFile", method = RequestMethod.POST)
	@ResponseBody
	MasterSchedule loadFromFile(
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res) throws IOException;
	
	@TrunkModuleFeature(id = "saveLimits", displayName = "Save Limits")
	@RequestMapping(value = "/saveLimits", method = RequestMethod.POST)
	@ResponseBody
	Limits saveLimits(

			@RequestParam(value = "Date", required = true) String date,
			@RequestParam(value = "Visit Limits", required = true) int limits,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
	
	@TrunkModuleFeature(id = "retLimits", displayName = "List Limits")
	@RequestMapping(value = "/retLimits", method = RequestMethod.POST)
	@ResponseBody
	List<Limits> retLimits(
			@RequestParam(value = "Date", required = true) String date,
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	
}