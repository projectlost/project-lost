package trunk.modules.masterschedule;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import trunk.interceptors.ApplicationContextProvider;

public class ReadFile {
	
	public MasterSchedule readFile() throws IOException {
		int count = 0;
		
		MasterSchedule ms = new MasterSchedule();
		FileInputStream fileInputStream = new FileInputStream("addr.xls");
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet worksheet = workbook.getSheet("Sheet1");
		
		do {
			HSSFRow row1 = worksheet.getRow(0);
			if(row1 == null) {
				count = 1;
				break;
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			HSSFCell cellA1 = row1.getCell((short) 0);
			HSSFCell cellB1 = row1.getCell((short) 1);
			HSSFCell cellC1 = row1.getCell((short) 2);
			HSSFCell cellD1 = row1.getCell((short) 3);
			HSSFCell cellE1 = row1.getCell((short) 4);
			HSSFCell cellF1 = row1.getCell((short) 5);
			HSSFCell cellG1 = row1.getCell((short) 6);
			HSSFCell cellH1 = row1.getCell((short) 7);
			HSSFCell cellI1 = row1.getCell((short) 8);
			HSSFCell cellJ1 = row1.getCell((short) 9);
			HSSFCell cellK1 = row1.getCell((short) 10);
			
			String a1Val = cellA1.getStringCellValue();
			double bVal = cellB1.getNumericCellValue();
			String b1Val = Double.toString(bVal);
			String c1Val = cellC1.getStringCellValue();
			String d1Val = cellD1.getStringCellValue();
			String e1Val = cellE1.getStringCellValue();
			String f1Val = cellF1.getStringCellValue();
			double gVal = cellG1.getNumericCellValue();
			String g1Val = Double.toString(gVal);
			Date hVal = cellH1.getDateCellValue();
			String h1Val = dateFormat.format(hVal); 
			String i1Val = cellI1.getStringCellValue();
			double jVal = cellJ1.getNumericCellValue();
			double kVal = cellK1.getNumericCellValue();
			
			MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);
			
			ms = mDao.saveProp(a1Val, b1Val, c1Val, d1Val, e1Val, f1Val, g1Val, 45, h1Val, i1Val, jVal, kVal);
			
			
		}while(count == 1);
		
		return ms;
		
	}

}
