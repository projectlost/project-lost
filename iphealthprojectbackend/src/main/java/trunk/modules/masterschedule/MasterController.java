package trunk.modules.masterschedule;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.MultiSideMenu;
import trunk.base.module.ui.sideMenu.SubSideMenu;
import trunk.base.module.ui.sideMenu.file.TrunkFile;
import trunk.interceptors.ApplicationContextProvider;

@Controller
public class MasterController extends TrunkModuleBase implements IMasterController {

	@Override
	public MasterSchedule saveProperty(String name, String unit, String street, String suburb, String city, String state, String postcode, String date, String usr, Double lat, Double lng, HttpServletRequest req, HttpServletResponse res) {

		User u = SessionProvider.getUser(req);
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);
		
		return mDao.saveProp(name, unit, street, suburb, city, state, postcode, u.getId(), date, usr, lat, lng);
	}
	
	@Override
	public List<MasterSchedule> listProperty(HttpServletRequest req) {

		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);

		return mDao.listProp();
	}
	
	@Override
	public Inspection saveInspection(int pid, String date, HttpServletRequest req, HttpServletResponse res) {

		User u = SessionProvider.getUser(req);
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);
		
		return mDao.saveIns(pid, date, u.getId());
	}
	
	@Override
	public List<MasterSchedule> retInspection(String date, HttpServletRequest req) {

		User u = SessionProvider.getUser(req);
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);

		return mDao.retIns(date, u.getName());
	}
	
	@Override
	public Inspection updInspection(int id) {
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);
		
		return mDao.updIns(id);
	}
	
	@Override
	public List<Inspection> listAllIns(int id) {
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);
		
		return mDao.listIns(id);
	}
	
	@Override
	public List<MasterSchedule> retInspectionStat(String date, HttpServletRequest req) {

		User u = SessionProvider.getUser(req);
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);

		return mDao.retInsStat(date, u.getName());
	}
	
	@Override
	public Limits saveLimits(String date, int limits, HttpServletRequest req, HttpServletResponse res) {

		User u = SessionProvider.getUser(req);
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);
		
		return mDao.saveLmt(u.getId(), u.getName(), date, limits);
	}
	
	@Override
	public List<Limits> retLimits(String date, HttpServletRequest req) {

		User u = SessionProvider.getUser(req);
		
		MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);

		return mDao.retLmt(date, u.getName());
	}
	
	@Override
	public void initializeUI(ModuleUI ui) {
		
		ui.addCssFile(new TrunkFile(ui, "/masterschedule.css"));
		ui.addJsFile(new TrunkFile(ui, "/masterschedule.js"));

		/*SingleSideMenu sm = new SingleSideMenu(ui);
		sm.setHref("/");
		sm.setName("Master Schedule");
		sm.setIcon("fa fa-calendar");
		sm.setTemplate("/masterschedule.html");
		sm.setId("masterMainSideSingle");

		ui.addSideMenu(sm);*/
		
		MultiSideMenu msm = new MultiSideMenu(ui);
		msm.setIcon("fa fa-calendar");
		msm.setName("Master Schedule");
		msm.setId("multiLocMainSideMulti");

		ui.addSideMenu(msm);

		SubSideMenu ssm = new SubSideMenu(ui);
		ssm.setName("Master Schedule");
		ssm.setHref("/masterschedule");
		ssm.setTemplate("/masterschedule.html");
		ssm.setId("multiLocMainSideMulti1");

		SubSideMenu ssm2 = new SubSideMenu(ui);
		ssm2.setName("Visit Limits");
		ssm2.setHref("/visitlimits");
		ssm2.setTemplate("/visitlimits.html");
		ssm2.setId("multiLocMainSideMulti2");
		
		SubSideMenu ssm3 = new SubSideMenu(ui);
		ssm3.setName("Properties");
		ssm3.setHref("/properties");
		ssm3.setTemplate("/properties.html");
		ssm3.setId("multiLocMainSideMulti3");

		msm.addSubSideMenu(ssm);
		msm.addSubSideMenu(ssm2);
		msm.addSubSideMenu(ssm3);
			
	}

	@Override
	public MasterSchedule loadFromFile(HttpServletRequest req, HttpServletResponse res)  {
		int count = 0;
		int i = 0;
		MasterSchedule ms = new MasterSchedule();
		Resource resource = new ClassPathResource("WEB-INF\\addrs_1.xls");
		InputStream resourceInputStream = null;
		try {
			resourceInputStream = resource.getInputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	
		HSSFWorkbook workbook = null;
		try {
			workbook = new HSSFWorkbook(resourceInputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		HSSFSheet worksheet = workbook.getSheet("Sheet1");
		do {
			HSSFRow row1 = worksheet.getRow(i);
			if(row1 == null) {
				count = 1;
				break;
			}
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			HSSFCell cellA1 = row1.getCell((short) 0);
			HSSFCell cellB1 = row1.getCell((short) 1);
			HSSFCell cellC1 = row1.getCell((short) 2);
			HSSFCell cellD1 = row1.getCell((short) 3);
			HSSFCell cellE1 = row1.getCell((short) 4);
			HSSFCell cellF1 = row1.getCell((short) 5);
			HSSFCell cellG1 = row1.getCell((short) 6);
			HSSFCell cellH1 = row1.getCell((short) 7);
			HSSFCell cellI1 = row1.getCell((short) 8);
			HSSFCell cellJ1 = row1.getCell((short) 9);
			HSSFCell cellK1 = row1.getCell((short) 10);
			
			String name = cellA1.getStringCellValue();
			Double bVal = cellB1.getNumericCellValue();
			String unit = Double.toString(bVal);
			String street = cellC1.getStringCellValue();
			String suburb = cellD1.getStringCellValue();
			String city = cellE1.getStringCellValue();
			String state = cellF1.getStringCellValue();
			Double gVal = cellG1.getNumericCellValue();
			String postcode = Double.toString(gVal);
			Date hVal = cellH1.getDateCellValue();
			String date = dateFormat.format(hVal); 
			String usr = cellI1.getStringCellValue();
			Double lat = cellJ1.getNumericCellValue();
			Double lng = cellK1.getNumericCellValue();
				
			MasterScheduleDao mDao = ApplicationContextProvider.getContext().getBean(MasterScheduleDao.class);
			
			ms = mDao.saveProp(name, unit, street, suburb, city, state, postcode, 45, date, usr, lat, lng);
			i++;
		}while(count == 0);
		
		return ms;
	}
}