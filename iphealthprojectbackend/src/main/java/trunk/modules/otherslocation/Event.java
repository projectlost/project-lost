package trunk.modules.otherslocation;

import java.util.Date;

public class Event {
	
	private int id;
	private int event_owner_id;
	private String event_owner;
	private String event_reciever;
	private Date event_time;
	
	public Event() {
		
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getOwnerId() {
		return this.event_owner_id;
	}
	
	public void setOwnerId(int owner_id) {
		this.event_owner_id = owner_id;
	}
	
	public String getOwnerName() {
		return this.event_owner;
	}
	
	public void setOwnerName(String name) {
		this.event_owner = name;
	}
	
	public String getRecieverName() {
		return this.event_reciever;
	}
	
	public void setRecieverName(String rec_name) {
		this.event_reciever = rec_name;
	}
	
	public Date getTime() {
		return this.event_time;
	}
	
	public void setTime(Date eTime) {
		this.event_time = eTime;
	}

}
