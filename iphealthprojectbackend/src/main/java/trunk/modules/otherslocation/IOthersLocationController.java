package trunk.modules.otherslocation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trunk.base.annotations.TrunkModuleFeature;
import trunk.base.annotations.TrunkWorkflowFeature;
import trunk.base.module.ITrunkModuleBase;

@RequestMapping("/otherslocation")
@TrunkModuleFeature(id = "otherslocation", displayName = "Others Location", addToDB = true, forSuperAdmin = false)
public interface IOthersLocationController extends ITrunkModuleBase {

	@TrunkWorkflowFeature(
			view = "/workflow/othersLocationWorkflow.html",
			cssFiles = {
					"/otherslocation.css",
			},
			jsFiles = {
					"/otherslocation.js",
			},
			settingsCssFiles = {
					"/otherslocation.css",
			},
			settings = "/workflow/settings.html",
			settingsJsFiles = { "/workflow/settings.js"
			})

	@TrunkModuleFeature(id = "saveLocation", displayName = "Save Location")
	@RequestMapping(value = "/saveLocation", method = RequestMethod.POST)
	@ResponseBody
	Others saveLocation(

			@RequestParam(value = "Lat", required = true) Double lat,
			@RequestParam(value = "Lng", required = true) Double lng,
			@RequestParam(value = "Url", required = true) String url,
			@RequestParam(value = "req", required = true) HttpServletRequest req,
			@RequestParam(value = "res", required = true) HttpServletResponse res);
	
	@TrunkModuleFeature(id = "listLocation", displayName = "List all locations")
	@RequestMapping(value = "/listLocation", method = RequestMethod.POST)
	@ResponseBody
	List<Others> listLocation(
			
			@RequestParam(value = "req", required = true) HttpServletRequest req);
	
	@TrunkModuleFeature(id = "listSpecLocation", displayName = "List specific locations")
	@RequestMapping(value="/listSpecLocation", method = RequestMethod.POST)
	@ResponseBody
	List<Event> listSpecLocation(
			@RequestParam(value = "req", required = true) HttpServletRequest req);

	
}