package trunk.modules.otherslocation;

public class Others {
	
	private int id;
	private int user_id;
	private String user_name;
	private Double lat;
	private Double lng;
	private String pic_url;
	
	public Others() {
		
	}
	
	public Others(Double lat, Double lng, String url) {
		this.lat = lat;
		this.lng = lng;
		this.pic_url = url;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	public String getName() {
		return this.user_name;
	}
	
	public void setLocation(String name) {
		this.user_name = name;
	}	
	
	public Double getLat() {
		return this.lat;
	}
	
	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	public Double getLng() {
		return this.lng;
	}
	
	public void setLng(Double lng) {
		this.lng = lng;
	}
	
	public String getUrl() {
		return this.pic_url;
	}
	
	public void setUrl(String url) {
		this.pic_url = url;
	}

}
