package trunk.modules.otherslocation;

import java.sql.Types;
import java.util.List;

import org.springframework.stereotype.Repository;

import trunk.base.data.base.JdbcDaoSupportBase;
import trunk.base.data.base.StoredProcParam;

/*
 * The database access object.
 * 
 * Must have the @Repository annotation and must extend JdbcDaoSupportBase
 * 
 * All database calls MUST be done using stored procedures.
 */
@Repository
public class OthersDao extends JdbcDaoSupportBase {

	/*
	 * Inserts data into others_location table
	 */
	public Others saveOthersLocation(String name, int uid, Double lat, Double lng, String url) {

		return invokeStoredProc(Others.class, "x_save_others_location",
				new StoredProcParam(Types.VARCHAR, "name", name),
				new StoredProcParam(Types.INTEGER, "uid", uid),
				new StoredProcParam(Types.DOUBLE, "lat", lat),
				new StoredProcParam(Types.DOUBLE, "lng", lng),
				new StoredProcParam(Types.VARCHAR, "url", url));
	}
	
	/*
	 * Retrieves data from the others_locatiion table based on the user id
	 */
	public List<Others> listOthersLocation(int uid) {

		return invokeStoredProcMulti(Others.class, "x_get_others_location",
				new StoredProcParam(Types.INTEGER, "uid", uid));
	}
	
	/*
	 * Retrieves data from the event table based on the user id
	 */
	public List<Event> listSpecificLocation(int uid) {

		return invokeStoredProcMulti(Event.class, "x_get_spec_location",
				new StoredProcParam(Types.INTEGER, "uid", uid));
	}

}