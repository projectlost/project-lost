package trunk.modules.otherslocation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;

import trunk.base.SessionProvider;
import trunk.base.auth.User;
import trunk.base.module.TrunkModuleBase;
import trunk.base.module.ui.ModuleUI;
import trunk.base.module.ui.sideMenu.SingleSideMenu;
import trunk.base.module.ui.sideMenu.file.TrunkFile;
import trunk.interceptors.ApplicationContextProvider;

/*
 * Every module should implement base interface (IMathsController) in this case and extend TrunkModuleBase.
 * 
 * @Controller annotation has to be present as well
 */
@Controller
public class OthersLocationController extends TrunkModuleBase implements IOthersLocationController {

	@Override
	public Others saveLocation(Double lat, Double lng, String url, HttpServletRequest req, HttpServletResponse res) {

		User u = SessionProvider.getUser(req);
		
		OthersDao olDao = ApplicationContextProvider.getContext().getBean(OthersDao.class);
		
		return olDao.saveOthersLocation(u.getName(), u.getId(), lat, lng, url);
	}

	/*
	 * Add side menu items for this module. Overridden method from
	 * TrunkModuleBase
	 */
	
	@Override
	public List<Others> listLocation(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);
		OthersDao olDao = ApplicationContextProvider.getContext().getBean(OthersDao.class);

		return olDao.listOthersLocation(u.getId());
	}
	
	@Override
	public List<Event> listSpecLocation(HttpServletRequest req) {
		User u = SessionProvider.getUser(req);
		OthersDao olDao = ApplicationContextProvider.getContext().getBean(OthersDao.class);

		return olDao.listSpecificLocation(u.getId());
	}
	
	@Override
	public void initializeUI(ModuleUI ui) {

		/*
		 * NOTE: All file URLS are relative to webapp/modules/<module id>
		 * directory
		 */
		
		ui.addCssFile(new TrunkFile(ui, "/otherslocation.css"));
		ui.addJsFile(new TrunkFile(ui, "/otherslocation.js"));

		SingleSideMenu sm = new SingleSideMenu(ui);

		/*
		 * URL of the page. This is the URL after http://localhost:8989/#/
		 */
		sm.setHref("/");
		sm.setName("Other Locations");
		sm.setIcon("fa fa-map-marker");
		/*
		 * View to load then this menu item is clicked
		 */
		sm.setTemplate("/otherslocation.html");
		sm.setId("fenceMainSideSingle");

		/*
		 * Attach the side menu to UI
		 */
		ui.addSideMenu(sm);
	}
}