package modules.messaging;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import trunk.modules.authentication.implementation.authentication.AuthController;
import trunk.modules.messaging.MessagingController;
import trunk.modules.messaging.beans.MessageBean;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:WEB-INF/views-servlet.xml")
@TransactionConfiguration(transactionManager = "transactionManagerJDBC", defaultRollback = true)
public class MessagingControllerTest {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Autowired
	private AuthController authController;

	@Autowired
	private MessagingController messagingController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		authController.loginWithoutReturn("iphealth", "test", "iphealth", request, response);
	}

	@After
	public void tearDown() {
		authController.logout(request);
	}

	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void send_Message() {
		int initialSize = messagingController.listMessages(request).size();

		messagingController.sendMessage("My name is Suresh. I love cricket. Sachin Tendulkar is my god and i don't like religion and caste stuffs.", request);

		List<MessageBean> messages = messagingController.listMessages(request);

		assertEquals(initialSize, messages.size() - 1);
	}
}
