package modules.masterschedule;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import trunk.modules.authentication.implementation.authentication.AuthController;
import trunk.modules.geofencing.GeoFence;
import trunk.modules.masterschedule.MasterController;
import trunk.modules.masterschedule.MasterSchedule;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:WEB-INF/views-servlet.xml")
@TransactionConfiguration(transactionManager = "transactionManagerJDBC", defaultRollback = true)
public class MasterControllerTest {
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Autowired
	private MasterController masterController;

	@Autowired
	private AuthController authController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		authController.loginWithoutReturn("iphealth", "test", "iphealth", request, response);
	}

	@After
	public void tearDown() {
		authController.logout(request);
	}

	@Rollback(true)
	@Transactional
	@Test
	public void is_User_Logged_In() {
		assertEquals(true, authController.isLoggedIn(request));
	}
	
	@Transactional
	@Test
	public void chk_file() throws IOException {
		
		int init_size = masterController.listProperty(request).size();
		
		masterController.loadFromFile(request, response);
		
		List<MasterSchedule> prop = masterController.listProperty(request);
		
		MasterSchedule ms = new MasterSchedule();
		
		ms = prop.get(prop.size() - 1);
		
		assertEquals(init_size, prop.size() - 1);
		
		
	}

}
