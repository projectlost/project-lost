package modules.maths;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import trunk.modules.authentication.implementation.authentication.AuthController;
import trunk.modules.maths.AddResult;
import trunk.modules.maths.MathsController;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:WEB-INF/views-servlet.xml")
@TransactionConfiguration(transactionManager = "transactionManagerJDBC", defaultRollback = true)
public class MathsControllerTest {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Autowired
	private MathsController mathsController;

	@Autowired
	private AuthController authController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		authController.loginWithoutReturn("iphealth", "test", "iphealth", request, response);
	}

	@After
	public void tearDown() {
		authController.logout(request);
	}

	@Rollback(true)
	@Transactional
	@Test
	public void is_User_Logged_In() {
		assertEquals(true, authController.isLoggedIn(request));
	}

	@Rollback(true)
	@Transactional
	@Test
	public void add_Numbers() {
		AddResult expect = new AddResult(1, 2, 3);
		AddResult result = mathsController.add(1, 2, request, response);

		assertEquals(expect.getNum1(), result.getNum1());
		assertEquals(expect.getSum(), result.getSum());
	}

	@Rollback(true)
	@Transactional
	@Test
	public void subtract_Numbers() {
		AddResult expect = new AddResult(1, 2, -1);
		AddResult result = mathsController.subtract(1, 2, request, response);

		assertEquals(expect.getNum1(), result.getNum1());
		assertEquals(expect.getSum(), result.getSum());

		expect = new AddResult(2, 1, 1);
		result = mathsController.subtract(2, 1, request, response);

		assertEquals(expect.getNum1(), result.getNum1());
		assertEquals(expect.getSum(), result.getSum());
	}
}
