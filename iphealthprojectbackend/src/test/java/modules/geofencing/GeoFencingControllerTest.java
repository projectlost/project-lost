package modules.geofencing;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import trunk.modules.authentication.implementation.authentication.AuthController;
import trunk.modules.geofencing.GeoFence;
import trunk.modules.geofencing.GeoFencingController;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:WEB-INF/views-servlet.xml")
@TransactionConfiguration(transactionManager = "transactionManagerJDBC", defaultRollback = true)
public class GeoFencingControllerTest {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Autowired
	private GeoFencingController geofenceController;

	@Autowired
	private AuthController authController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		authController.loginWithoutReturn("iphealth", "test", "iphealth", request, response);
	}

	@After
	public void tearDown() {
		authController.logout(request);
	}

	@Rollback(true)
	@Transactional
	@Test
	public void is_User_Logged_In() {
		assertEquals(true, authController.isLoggedIn(request));
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test
	public void drawFence_db_insert() {
		
		int init_size = geofenceController.listFences(request).size();
		
		geofenceController.drawFence("Home", "49 Rosemount Tce", 0.3, -27.00, 153.00, request, response);
		
		List<GeoFence> fences = geofenceController.listFences(request);
		
		assertEquals(init_size, fences.size() - 1);
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test
	public void drawFence_db_insert_correct() {
		
		Double rad = 0.3;
		Double lat = -27.00;
		Double lng = 153.00;
		geofenceController.drawFence("Home", "49 Rosemount Tce", rad, lat, lng, request, response);
		
		List<GeoFence> fences = geofenceController.listFences(request);
		
		GeoFence fence = new GeoFence();
		
		fence = fences.get(fences.size() - 1);
		
		assertEquals("Home", fence.getName());
		assertEquals("49 Rosemount Tce", fence.getLocation());
		assertEquals(rad, fence.getRadius());
		assertEquals(lat, fence.getLat());
		assertEquals(lng, fence.getLng());
		
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void drawFence_db_error_name() {
		
		geofenceController.drawFence("Home sweet home. University hectic. Cricket stadium is fun", "49 Rosemount Tce", 0.3, -27.00, 153.00, request, response);
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void drawFence_db_error_location() {
		
		geofenceController.drawFence("Home", "49 Rosemount Tce, Brisbane, Queensland, Australia, World, Universe", 0.3, -27.00, 153.00, request, response);
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void drawFence_db_null_name() {
		
		geofenceController.drawFence(null, "49 Rosemount Tce", 0.3, -27.00, 153.00, request, response);
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void drawFence_db_null_location() {
		
		geofenceController.drawFence("Home", null, 0.3, -27.00, 153.00, request, response);
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void drawFence_db_null_radius() {
		
		geofenceController.drawFence("Home", "49 Rosemount Tce", null, -27.00, 153.00, request, response);
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void drawFence_db_null_lat() {
		
		geofenceController.drawFence("Home", "49 Rosemount Tce", 0.2, null, 153.00, request, response);
		
	}
	
	@Rollback(true)
	@Transactional(rollbackFor = { Exception.class })
	@Test(expected = DataIntegrityViolationException.class)
	public void drawFence_db_null_lng() {
		
		geofenceController.drawFence("Home", "49 Rosemount Tce", 0.2, -27.00, null, request, response);
		
	}


	
}
