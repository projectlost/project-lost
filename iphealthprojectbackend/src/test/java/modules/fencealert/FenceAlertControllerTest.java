package modules.fencealert;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import trunk.modules.authentication.implementation.authentication.AuthController;
import trunk.modules.fencealert.Alert;
import trunk.modules.fencealert.FenceAlertController;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:WEB-INF/views-servlet.xml")
@TransactionConfiguration(transactionManager = "transactionManagerJDBC", defaultRollback = true)
public class FenceAlertControllerTest {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;

	@Autowired
	private FenceAlertController fenceController;

	@Autowired
	private AuthController authController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();

		authController.loginWithoutReturn("iphealth", "test", "iphealth", request, response);
	}

	@After
	public void tearDown() {
		authController.logout(request);
	}

	@Rollback(true)
	@Transactional
	@Test
	public void is_User_Logged_In() {
		assertEquals(true, authController.isLoggedIn(request));
	}

	@Rollback(true)
	@Transactional
	@Test
	public void alert_Location() {

		Alert expect = new Alert(-27.00, 153.00);
		Alert result = fenceController.alertFence(-27.00, 153.00, request, response);

		assertEquals(expect.getLat(), result.getLat());
		assertEquals(expect.getLng(), result.getLng());
	}
}

