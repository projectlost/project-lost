-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2015 at 12:50 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iphealthproject`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_branch`(IN p_parent_uid int,
														 IN p_tenant_name varchar(45),
														 IN p_tenant_unique_name varchar(45),
														 IN p_firstname varchar(250),
														 IN p_username varchar(250),
														 IN p_password varchar(250))
BEGIN

DECLARE exit handler for sqlexception 
  BEGIN
    -- ERROR
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'Branch with that unique name already exists';
  ROLLBACK;
END;

DECLARE exit handler for sqlwarning
 BEGIN
    -- WARNING
 ROLLBACK;
END;

START TRANSACTION;

	select	count(*) 
	into 	@unameexists 
	from 	`branch` 
	where 	unique_name = p_tenant_unique_name;

	if @unameexists > 0
	then
		SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'Branch with that unique name already exists';
	else
		select branch_id into @parent_branch_id from user where user.id = p_parent_uid;
		select tenant_id into @tenant_id from branch where branch.id = @parent_branch_id;

		INSERT 	INTO `branch`(`parent_branch_id`,`name`,`unique_name`,`tenant_id`)
		VALUES	(@parent_branch_id,p_tenant_name,p_tenant_unique_name, @tenant_id);

		SELECT LAST_INSERT_ID() into @branch_id;

		INSERT 	INTO `user`(`user_name`,`name`,`branch_id`,`is_super_user`,`password`)
		VALUES	(p_username,p_firstname,@branch_id,true,p_password);

		SELECT LAST_INSERT_ID() into @user_id;

		SELECT @branch_id as branch_id, @user_id as user_id;
	end if;

COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `alluser`()
BEGIN

select * from user;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `assign_group_to_user`(IN p_branch_id INT,
																  IN p_group_id INT,
																  IN p_uid INT,
																  IN p_assign BOOLEAN)
BEGIN

	select	count(*) 
	into 	@groupexistsinbranch 
	from 	`group` 
	where 	id = p_group_id
			and branch_id = p_branch_id;

	if @groupexistsinbranch <= 0
	then
		SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'That group doesn\'t exist in branch';
	end if;

	select	count(*) 
	into 	@userexistsinbranch 
	from 	`user` 
	where 	id = p_uid
			and branch_id = p_branch_id;

	if @userexistsinbranch <= 0
	then
		SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'That user doesn\'t exist in branch';
	end if;

	if p_assign is true
	then
		insert ignore into user_group	(user_id, group_id)
		values							(p_uid, p_group_id);
	else
		delete from user_group
		where		user_id = p_uid
					and group_id = p_group_id;
	end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `assign_role_to_user`(IN p_branch_id INT,
																  IN p_role_id INT,
																  IN p_uid INT,
																  IN p_assign BOOLEAN)
BEGIN

	select	count(*) 
	into 	@roleexistsinbranch 
	from 	`role` 
	where 	id = p_role_id
			and branch_id = p_branch_id;

	if @roleexistsinbranch <= 0
	then
		SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'That role doesn\'t exist in branch';
	end if;

	select	count(*) 
	into 	@userexistsinbranch 
	from 	`user` 
	where 	id = p_uid
			and branch_id = p_branch_id;

	if @userexistsinbranch <= 0
	then
		SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'That user doesn\'t exist in branch';
	end if;

	if p_assign is true
	then
		insert ignore into user_role	(user_id, role_id)
		values							(p_uid, p_role_id);
	else
		delete from user_role
		where		user_id = p_uid
					and role_id = p_role_id;
	end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_group`(IN p_branch_id INT,
								IN p_name VARCHAR(45),
								IN p_description VARCHAR(250))
BEGIN

select	count(*) 
into 	@nameexists 
from 	`group` 
where 	group.name = p_name
		and group.branch_id = p_branch_id;

if @nameexists >= 1
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'group with that name already exists';
end if;

insert 	into `group`(`name`,`description`,`branch_id`)
values			   (p_name,p_description, p_branch_id);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_role`(IN p_branch_id INT,
								IN p_name VARCHAR(45),
								IN p_description VARCHAR(250),
								IN p_parent_role INT)
BEGIN

select	count(*) 
into 	@nameexists 
from 	role 
where 	role.name = p_name
		and role.branch_id = p_branch_id;

if @nameexists >= 1
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'role with that name already exists';
end if;

select	count(*)
into	@parentInBranch
from	role
where	role.id = p_parent_role
		and role.branch_id = p_branch_id;

if @parentInBranch <= 0 and p_parent_role is not null
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'parent role does not exist in current branch';
end if;

insert 	into `role`(`name`,`description`,`branch_id`,`parent_role_id`)
values			   (p_name,p_description, p_branch_id, p_parent_role);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_tenant`(IN p_tenant_name varchar(45),
															IN p_tenant_unique_name varchar(45),
															IN p_firstname varchar(250),
															IN p_username varchar(250),
															IN p_password varchar(250))
BEGIN

DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'Branch with that unique name already exists';
  ROLLBACK;
END;

DECLARE exit handler for sqlwarning
 BEGIN
    -- WARNING
 ROLLBACK;
END;

START TRANSACTION;

	select	count(*) 
	into 	@unameexists 
	from 	`branch` 
	where 	unique_name = p_tenant_unique_name;

	if @unameexists > 0
	then
		SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'Branch with that unique name already exists';
	else
		INSERT 	INTO `tenant`(`name`)
		VALUES	(p_tenant_name);

		SELECT LAST_INSERT_ID() into @tenant_id;

		INSERT 	INTO `branch`(`parent_branch_id`,`name`, `unique_name`,`tenant_id`)
		VALUES	(null,p_tenant_name,p_tenant_unique_name, @tenant_id);

		SELECT LAST_INSERT_ID() into @branch_id;

		INSERT 	INTO `user`(`user_name`,`name`,`branch_id`,`is_super_user`,`password`)
		VALUES	(p_username,p_firstname,@branch_id,true,p_password);
	end if;

COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user`(IN p_branch_id INT,
														  IN p_super_admin BOOLEAN,
														  IN p_firstname varchar(250),
														  IN p_username varchar(250),
														  IN p_password varchar(250))
BEGIN


select	count(*) 
into 	@unameexists 
from 	user 
where 	branch_id = p_branch_id
		and user_name = p_username;

if @unameexists > 0
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'User already exists in branch';
else
	INSERT 	INTO `user`(`user_name`,`name`,`branch_id`,`is_super_user`,`password`)
	VALUES	(p_username,p_firstname,p_branch_id,p_super_admin,p_password);
end if;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_group`(IN p_branch_id INT,
																IN p_name VARCHAR(45))
BEGIN
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
END;

DECLARE exit handler for sqlwarning
 BEGIN
    -- WARNING
 ROLLBACK;
END;

START TRANSACTION;

delete from `group` where `name` = p_name and branch_id = p_branch_id;

COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_role`(IN p_branch_id INT,
																IN p_name VARCHAR(45))
BEGIN
DECLARE exit handler for sqlexception
  BEGIN
    -- ERROR
  ROLLBACK;
END;

DECLARE exit handler for sqlwarning
 BEGIN
    -- WARNING
 ROLLBACK;
END;

START TRANSACTION;

delete from role where `name` = p_name and branch_id = p_branch_id;

COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_user`(IN p_branch_id INT,
														  IN p_c_uid INT,
														  IN p_d_uid INT)
BEGIN

if p_c_uid = p_d_uid
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'You can\'t delete yourself';
end if;

select	count(*) 
into 	@unameexists 
from 	user 
where 	branch_id = p_branch_id
		and id = p_d_uid;

if @unameexists <= 0
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'User doen\'t exist in branch';
else
	delete 	from user
	where	branch_id = p_branch_id
			and id = p_d_uid;
end if;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_branch_groups`(IN p_branch_id INT)
BEGIN

select	*
from 	`group` 
where 	branch_id = p_branch_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_branch_roles`(IN p_branch_id INT)
BEGIN

select	*
from 	role 
where 	role.branch_id = p_branch_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_users`(IN p_branch_id INT,
														IN p_page_count INT,
														IN p_page_number INT)
BEGIN

	declare _totalPagesCount int;
	declare _startIndex int;
	declare _totalRecordsCount int;
	
	select count(*) from user where branch_id = p_branch_id into _totalRecordsCount;
	set _startIndex = ((p_page_number-1) * p_page_count);
	set _totalPagesCount = ceil(_totalRecordsCount / p_page_count);
		

	select	id, user_name,name,branch_id,is_super_user, _totalPagesCount as totalpages, _totalRecordsCount as totalrecords
	from 	user
	where 	branch_id = p_branch_id
	limit 	_startIndex, p_page_count;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_assigned_groups`(IN p_branch_id INT,
																	   IN p_uid INT)
BEGIN

select	id, name, description,
		id in(
			select 	group_id
			from	user_group
			where	user_id = p_uid
		) as assigned
from	`group`
where 	branch_id = p_branch_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_assigned_roles`(IN p_branch_id INT,
																	  IN p_uid INT)
BEGIN

select	id, name, description,
		id in(
			select 	role_id
			from	user_role
			where	user_id = p_uid
		) as assigned
from	role
where 	branch_id = p_branch_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_immediate_branches`(IN p_bid INT)
BEGIN

select	*
from 	branch
where	branch.parent_branch_id = p_bid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_loggedin_user_details`(IN p_uid INT)
BEGIN

select 		branch.unique_name as unique_name, branch.name as branch_name , user.name as first_name, user.is_super_user as is_super_admin 
from 		user
inner join	branch
on 			user.branch_id = branch.id
where 		user.id = p_uid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `login`(IN p_user_name VARCHAR(45),
													IN p_password VARCHAR(250),
													IN p_branch_unique_name VARCHAR(45))
BEGIN

/*SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'oops';*/

select id into @branch_id from branch where unique_name = p_branch_unique_name;

select		* 
from 		user 
where 		user_name = p_user_name
			and password = p_password
			and branch_id = @branch_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `search_user_in_branch`(IN p_bid INT,
																	IN p_query VARCHAR(45))
BEGIN

declare toSearch varchar(250);
set toSearch = concat('%',p_query,'%');

select	id, `name`, user_name
from	user
where	user_name like toSearch
		or `name` like toSearch
limit	5;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_group`(IN p_branch_id INT,
								IN p_role_id INT,
								IN p_name VARCHAR(45),
								IN p_description VARCHAR(250))
BEGIN

select	count(*) 
into 	@nameexists 
from 	`group` 
where 	group.name = p_name
		and group.id != p_role_id
		and group.branch_id = p_branch_id;

if @nameexists >= 1
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'group with that name already exists';
end if;


update	`group`
set		group.name = p_name,
		group.description = p_description
where	group.id = p_role_id
		and group.branch_id = p_branch_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_role`(IN p_branch_id INT,
								IN p_role_id INT,
								IN p_name VARCHAR(45),
								IN p_description VARCHAR(250),
								IN p_parent_role INT)
BEGIN

select	count(*) 
into 	@nameexists 
from 	role 
where 	role.name = p_name
		and role.id != p_role_id
		and role.branch_id = p_branch_id;

if @nameexists >= 1
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'role with that name already exists';
end if;

select	count(*)
into	@parentInBranch
from	role
where	role.id = p_parent_role
		and role.branch_id = p_branch_id;

if @parentInBranch <= 0 and p_parent_role is not null
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'parent role does not exist in current branch';
end if;

update	role
set		role.name = p_name,
		role.description = p_description,
		role.parent_role_id = p_parent_role
where	role.id = p_role_id
		and role.branch_id = p_branch_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user`(IN p_branch_id INT,
														  IN p_c_uid INT,
														  IN p_u_uid INT,
														  IN p_super_admin BOOLEAN,
														  IN p_firstname varchar(250))
BEGIN

select	is_super_user
into 	@meisadmin 
from 	user
where 	branch_id = p_branch_id
		and id = p_c_uid;

if @meisadmin is true and p_c_uid = p_u_uid and p_super_admin is not true
then
	SIGNAL SQLSTATE 'ERR0R' SET MESSAGE_TEXT = 'You cannot revoke your own super admin status';
else
	update	user
	set		`name` = p_firstname,
			is_super_user = p_super_admin
	where 	id = p_u_uid
			and branch_id = p_branch_id;
end if;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_alert_fence`(IN `uid` INT)
    NO SQL
BEGIN

select * from geo_fence;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_all_fences`(IN `uid` INT)
BEGIN

select * from geo_fence;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_all_messages`(IN uid INT)
BEGIN

select * from message;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_all_property`()
    NO SQL
BEGIN

select * from property;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_group_details`(IN `u_name` VARCHAR(100), IN `grp_no` VARCHAR(10))
    NO SQL
BEGIN

select * from address_details where `user` = u_name and `group_no` = grp_no;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_ins_details`(IN `dt` VARCHAR(11), IN `usr` VARCHAR(15))
    NO SQL
BEGIN

select i.id, p.name, p.unit, p.street, 
p.suburb, p.city, p.state, p.post,
p.uid,i.date,p.assigned_user 
from property p,inspection_details i 
where p.id = i.pid 
and i.date = dt 
and p.assigned_user = usr;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_ins_details_status`(IN `dt` VARCHAR(11), IN `usr` VARCHAR(15))
    NO SQL
BEGIN

select i.id, p.name, p.unit, p.street, 
p.suburb, p.city, p.state, p.post,
p.uid,i.date,p.assigned_user,p.lat,p.lng 
from property p,inspection_details i 
where p.id = i.pid 
and i.date = dt 
and p.assigned_user = usr
and i.status != 'OK';

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_multiple_location`(IN `u_name` VARCHAR(100))
    NO SQL
BEGIN

SELECT * FROM address_details where `user` = u_name GROUP BY group_no;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_others_location`(IN `uid` INT)
    NO SQL
BEGIN

SELECT other1.*
FROM others_location other1 LEFT JOIN others_location other2
 ON (other1.user_name = other2.user_name AND other1.id < other2.id)
WHERE other2.id IS NULL;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_spec_location`(IN `uid` INT)
    NO SQL
BEGIN

SELECT e1.*
FROM event e1 LEFT JOIN event e2
 ON (e1.event_reciever = e2.event_reciever AND e1.id < e2.id)
WHERE e2.id IS NULL and e1.event_owner_id = uid and TIME_TO_SEC(TIMEDIFF(e1.event_time, sysdate())) < 900 and e1.event_time > sysdate();


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_get_users`(IN `uid` INT)
    NO SQL
BEGIN

select * from user;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_list_all_inspection`(IN `i_id` INT)
    NO SQL
BEGIN

select * from inspection_details
where pid = i_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_ret_limits`(IN `dt` VARCHAR(11), IN `usr` VARCHAR(15))
    NO SQL
BEGIN

select * from visit_limits where `date` = dt and `user_name` = usr;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_save_a_fence`(IN `name` VARCHAR(30), IN `uid` INT, IN `loc` VARCHAR(35), IN `rad` DOUBLE, IN `lat` DOUBLE, IN `lng` DOUBLE)
BEGIN

insert into `geo_fence`(`name`,`user_id`,`location`,`radius`,`lat`,`lng`)
values				 (name, uid, loc, rad, lat, lng);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_save_a_property`(IN `name` VARCHAR(250), IN `unit` VARCHAR(250), IN `street` VARCHAR(250), IN `suburb` VARCHAR(250), IN `city` VARCHAR(250), IN `state` VARCHAR(250), IN `postcode` VARCHAR(250), IN `uid` INT, IN `dt` VARCHAR(11), IN `a_user` VARCHAR(15), IN `lt` DOUBLE, IN `lg` DOUBLE)
    NO SQL
BEGIN

insert into `property`(`name`,`unit`,`street`,`suburb`,`city`,`state`,`post`,`uid`,`date`,`assigned_user`,`lat`, `lng`)
values				 (name, unit, street, suburb, city, state, postcode, uid, dt,a_user,lt,lg);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_save_inspection`(IN `pid` INT, IN `dt` VARCHAR(11), IN `uid` INT)
    NO SQL
BEGIN

insert into `inspection_details`(`pid`,`date`,`uid`)
values				 (pid, dt, uid);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_save_limits`(IN `u_id` INT, IN `u_name` VARCHAR(15), IN `dt` VARCHAR(11), IN `lmt` INT)
    NO SQL
BEGIN

insert into `visit_limits`(`user_id`,`user_name`,`date`,`limits`)
values				 (u_id, u_name, dt, lmt);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_save_multiple_location`(IN `uid` INT, IN `address` VARCHAR(300), IN `user_name` VARCHAR(100), IN `grp_no` VARCHAR(10))
    NO SQL
BEGIN

insert into `address_details`(`user_id`,`address`,`user`,`group_no`)
values				 (uid, address, user_name, grp_no);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_save_others_location`(IN `name` VARCHAR(250), IN `uid` INT, IN `lat` DOUBLE, IN `lng` DOUBLE, IN `url` VARCHAR(250))
BEGIN

insert into `others_location`(`user_name`,`user_id`,`lat`,`lng`,`pic_url`)
values				 (name, uid, lat, lng, url);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_send_a_message`(IN uid INT,
									 IN mess varchar(45))
BEGIN

insert into `message`(`message`,`user_id`)
values				 (mess, uid);

SELECT LAST_INSERT_ID() into @messageid;

select * from message where id = @messageid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `x_upd_inspection`(IN `i_id` INT)
    NO SQL
BEGIN

update inspection_details set status = 'OK'
where id = i_id;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `address_details`
--

CREATE TABLE IF NOT EXISTS `address_details` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(300) NOT NULL,
  `user` varchar(100) NOT NULL,
  `group_no` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address_details`
--

INSERT INTO `address_details` (`id`, `user_id`, `address`, `user`, `group_no`) VALUES
(16, 45, 'Brisbane', 'Suresh', '1'),
(17, 45, 'Sydney', 'Suresh', '1'),
(18, 45, 'Perth', 'Suresh', '2'),
(19, 45, 'Sydney', 'Suresh', '2'),
(20, 45, '29 Raymont Road, Grange QLD', 'IronMan', '1'),
(21, 45, '94 Northgate Road, Northgate QLD', 'IronMan', '1');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
`id` int(11) NOT NULL,
  `parent_branch_id` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  `unique_name` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `parent_branch_id`, `name`, `tenant_id`, `unique_name`) VALUES
(39, NULL, 'iphealth', 28, 'iphealth');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
`id` int(11) NOT NULL,
  `event_owner_id` int(11) NOT NULL,
  `event_owner` varchar(250) NOT NULL,
  `event_reciever` varchar(250) NOT NULL,
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `event_owner_id`, `event_owner`, `event_reciever`, `event_time`) VALUES
(1, 45, 'IPAdmin', 'IronMan', '2015-01-09 08:00:00'),
(2, 45, 'IPAdmin', 'Batman', '2015-01-09 08:00:00'),
(3, 45, 'IPAdmin', 'Suresh', '2015-01-09 06:00:00'),
(4, 45, 'IPAdmin', 'Suresh', '2015-01-09 06:45:00'),
(5, 45, 'IPAdmin', 'Suresh', '2015-01-09 07:09:00'),
(6, 45, 'IPAdmin', 'Batman', '2015-01-09 07:30:00'),
(7, 45, 'IPAdmin', 'Suresh', '2015-01-09 09:20:00'),
(8, 45, 'IPAdmin', 'Suresh', '2015-01-09 09:25:00'),
(9, 45, 'IPAdmin', 'Batman', '2015-01-12 04:05:00'),
(10, 46, 'Suresh', 'IronMan', '2015-01-12 04:05:00'),
(11, 45, 'IPAdmin', 'Batman', '2015-01-19 03:25:00'),
(12, 46, 'Suresh', 'IPAdmin', '2015-01-19 03:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `geo_fence`
--

CREATE TABLE IF NOT EXISTS `geo_fence` (
`id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `location` varchar(35) NOT NULL,
  `radius` double NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `geo_fence`
--

INSERT INTO `geo_fence` (`id`, `name`, `user_id`, `location`, `radius`, `lat`, `lng`) VALUES
(26, 'Home', 45, '49 Rosemount Tce', 200, -27.435667, 153.03078200000004),
(27, 'Cricket', 45, 'Wollongabba', 150, -27.5005578, 153.03532080000002),
(44, 'University', 45, 'QUT', 400, -27.4771379, 153.0284355),
(45, 'Happy', 46, 'Chennai', 400, 13.081604, 80.27518280000004),
(46, 'Party', 45, 'Sydney', 300, -33.8674869, 151.20699020000006),
(47, 'Home', 45, 'Chennai', 500, 13.0826802, 80.27071840000008);

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE IF NOT EXISTS `group` (
`id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group`
--

INSERT INTO `group` (`id`, `name`, `description`, `branch_id`) VALUES
(10, 'group1', '', 39),
(11, 'group2', '', 39);

-- --------------------------------------------------------

--
-- Table structure for table `inspection_details`
--

CREATE TABLE IF NOT EXISTS `inspection_details` (
`id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `date` varchar(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_details`
--

INSERT INTO `inspection_details` (`id`, `pid`, `date`, `uid`, `status`) VALUES
(95, 28, '2015-02-16', 45, 'OK'),
(96, 28, '2015-08-10', 45, ''),
(97, 28, '2015-11-10', 45, ''),
(98, 29, '2015-02-16', 45, 'OK'),
(99, 29, '2015-08-10', 45, ''),
(100, 29, '2015-11-10', 45, ''),
(101, 30, '2015-08-10', 45, ''),
(102, 30, '2015-02-16', 45, 'OK'),
(103, 30, '2015-11-10', 45, ''),
(104, 31, '2015-11-10', 45, ''),
(105, 31, '2015-02-12', 45, ''),
(106, 31, '2015-08-10', 45, ''),
(107, 32, '2015-08-10', 45, ''),
(108, 32, '2015-11-10', 45, ''),
(109, 32, '2015-02-17', 45, 'OK'),
(110, 33, '2015-08-10', 45, ''),
(111, 33, '2015-11-10', 45, ''),
(112, 33, '2015-02-17', 45, 'OK'),
(113, 34, '2015-08-10', 45, ''),
(114, 34, '2015-11-10', 45, ''),
(115, 34, '2015-02-17', 45, 'OK'),
(116, 35, '2015-02-28', 45, ''),
(117, 35, '2015-05-13', 45, ''),
(118, 35, '2015-02-12', 45, ''),
(119, 36, '2015-02-16', 45, 'OK'),
(120, 36, '2015-02-28', 45, ''),
(121, 36, '2015-11-11', 45, ''),
(122, 37, '2015-08-11', 45, ''),
(123, 37, '2015-02-28', 45, ''),
(124, 37, '2015-02-17', 45, 'OK'),
(125, 38, '2015-05-18', 45, ''),
(126, 38, '2015-08-16', 45, ''),
(127, 38, '2015-11-15', 45, ''),
(128, 45, '2015-02-20', 45, ''),
(129, 45, '2015-02-20', 45, ''),
(130, 45, '2015-02-20', 45, ''),
(131, 46, '2015-02-22', 45, 'OK'),
(132, 46, '2015-02-22', 45, 'OK'),
(133, 46, '2015-02-22', 45, ''),
(134, 47, '2015-02-24', 45, ''),
(135, 47, '2015-02-24', 45, ''),
(136, 47, '2015-02-24', 45, ''),
(137, 54, '2015-05-31', 45, ''),
(138, 54, '2015-11-27', 45, ''),
(139, 54, '2015-08-27', 45, '');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
`id` int(11) NOT NULL,
  `message` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `message`, `user_id`) VALUES
(10, 'A basic demo module', 45),
(11, 'Hello', 45),
(12, 'Hello', 45),
(13, 'Are yu ok ????', 45),
(14, 'I''m waiting...', 45);

-- --------------------------------------------------------

--
-- Table structure for table `others_location`
--

CREATE TABLE IF NOT EXISTS `others_location` (
`id` int(11) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `pic_url` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `others_location`
--

INSERT INTO `others_location` (`id`, `user_name`, `user_id`, `lat`, `lng`, `pic_url`) VALUES
(13, 'IPAdmin', 45, -27.435600599999997, 153.0304641, 'core/img/avatar.gif'),
(14, 'Suresh', 46, -27.4356604, 153.03050819999999, 'core/img/avatar.gif'),
(15, 'Suresh', 46, -37.4355655, 153.03049000000001, 'core/img/avatar.gif'),
(16, 'IronMan', 47, -27.4356504, 153.0305305, 'core/img/avatar.gif'),
(17, 'IronMan', 47, -27.4356255, 153.0305548, 'core/img/avatar.gif'),
(18, 'Batman', 48, -27.435648, 153.0305285, 'core/img/avatar.gif'),
(19, 'Batman', 48, -27.4355889, 153.0305673, 'core/img/avatar.gif'),
(25, 'IPAdmin', 45, -27.4356202, 153.0305716, 'core/img/avatar.gif'),
(26, 'IPAdmin', 45, -27.4356202, 153.0305716, 'core/img/avatar.gif'),
(27, 'IPAdmin', 45, -27.4356202, 153.0305716, 'core/img/avatar.gif'),
(28, 'IPAdmin', 45, -27.4356202, 153.0305716, 'core/img/avatar.gif'),
(29, 'IPAdmin', 45, -27.4356202, 153.0305716, 'core/img/avatar.gif'),
(30, 'IPAdmin', 45, -27.4356202, 153.0305716, 'core/img/avatar.gif'),
(31, 'IPAdmin', 45, -27.4356513, 153.0305811, 'core/img/avatar.gif'),
(32, 'IPAdmin', 45, -27.4356565, 153.0306761, 'core/img/avatar.gif'),
(33, 'IPAdmin', 45, -27.4356565, 153.0306761, 'core/img/avatar.gif'),
(34, 'Suresh', 46, -27.4355909, 153.03063269999998, 'core/img/avatar.gif'),
(35, 'IPAdmin', 45, -27.435590100000002, 153.03063459999998, 'core/img/avatar.gif'),
(36, 'IPAdmin', 45, -27.435589800000002, 153.0306197, 'core/img/avatar.gif'),
(37, 'IPAdmin', 45, -27.435589800000002, 153.0306197, 'core/img/avatar.gif'),
(38, 'IPAdmin', 45, -27.435590100000002, 153.03063459999998, 'core/img/avatar.gif'),
(39, 'IPAdmin', 45, -27.435590100000002, 153.03063459999998, 'core/img/avatar.gif'),
(40, 'IPAdmin', 45, -27.435590100000002, 153.03063459999998, 'core/img/avatar.gif'),
(41, 'IPAdmin', 45, -27.435590100000002, 153.03063459999998, 'core/img/avatar.gif'),
(42, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(43, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(44, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(45, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(46, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(47, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(48, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(49, 'IPAdmin', 45, -27.4355911, 153.0306377, 'core/img/avatar.gif'),
(50, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(51, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(52, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(53, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(54, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(55, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(56, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(57, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(58, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(59, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(60, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(61, 'IPAdmin', 45, -27.435620399999998, 153.03062889999998, 'core/img/avatar.gif'),
(62, 'IPAdmin', 45, -27.435620399999998, 153.03062889999998, 'core/img/avatar.gif'),
(63, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(64, 'IPAdmin', 45, -27.435620399999998, 153.03062889999998, 'core/img/avatar.gif'),
(65, 'IPAdmin', 45, -27.435630999999997, 153.0306056, 'core/img/avatar.gif'),
(66, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(67, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(68, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(69, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(70, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(71, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(72, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(73, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(74, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(75, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(76, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(77, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(78, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(79, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(80, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(81, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(82, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(83, 'IPAdmin', 45, -27.4356412, 153.03059149999999, 'core/img/avatar.gif'),
(84, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(85, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(86, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(87, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(88, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(89, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(90, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(91, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(92, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(93, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(94, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(95, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(96, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(97, 'IPAdmin', 45, -27.435638899999997, 153.0306191, 'core/img/avatar.gif'),
(98, 'IPAdmin', 45, -27.435585, 153.03050629999998, 'core/img/avatar.gif'),
(99, 'IPAdmin', 45, -27.4355767, 153.03065089999998, 'core/img/avatar.gif'),
(100, 'IPAdmin', 45, -27.435574499999998, 153.0306737, 'core/img/avatar.gif'),
(101, 'IPAdmin', 45, -27.4355767, 153.03065089999998, 'core/img/avatar.gif'),
(102, 'IPAdmin', 45, -27.4355767, 153.03065089999998, 'core/img/avatar.gif'),
(103, 'IPAdmin', 45, -27.4355767, 153.03065089999998, 'core/img/avatar.gif'),
(104, 'IPAdmin', 45, -27.435534699999998, 153.0306823, 'core/img/avatar.gif'),
(105, 'Suresh', 46, -27.435542299999998, 153.0306994, 'core/img/avatar.gif'),
(106, 'Suresh', 46, -27.4355653, 153.0305672, 'core/img/avatar.gif');

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE IF NOT EXISTS `property` (
`id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `unit` varchar(250) DEFAULT NULL,
  `street` varchar(250) DEFAULT NULL,
  `suburb` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `post` varchar(250) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `date` varchar(11) NOT NULL,
  `assigned_user` varchar(15) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `name`, `unit`, `street`, `suburb`, `city`, `state`, `post`, `uid`, `date`, `assigned_user`, `lat`, `lng`) VALUES
(28, 'Suresh', '16/49', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-11', 'IronMan', -27.4368363, 153.03108989999998),
(29, 'Venu', '18/49', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-11', 'IronMan', -27.4368363, 153.03108989999998),
(30, 'Hari', '5/49', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-11', 'IronMan', -27.4368363, 153.03108989999998),
(31, 'Surya', '7/49', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-11', 'IronMan', -27.4368363, 153.03108989999998),
(32, 'Virat', '55', 'Sangate Rd', 'Albion', 'Brisbane', 'QLD', '4056', 45, '2015-02-11', 'IronMan', -27.4333802, 153.0428774),
(33, 'MSD', '67', 'Sangate Rd', 'Albion', 'Brisbane', 'QLD', '4056', 45, '2015-02-11', 'IronMan', -27.4333802, 153.0428774),
(34, 'Raina', '98', 'Sanagte Rd', 'Albion', 'Brisbane', 'QLD', '4056', 45, '2015-02-11', 'IronMan', -27.4333802, 153.0428774),
(35, 'Rohit', '1', 'Sanagte Rd', 'Albion', 'Brisbane', 'QLD', '4056', 45, '2015-02-11', 'IronMan', -27.4333802, 153.0428774),
(36, 'Shami', '94', 'Northgate Rd', 'Northgate', 'Brisbane', 'QLD', '4025', 45, '2015-02-12', 'IronMan', -27.390002, 153.0733509),
(37, 'Bhuvi', '67', 'Northgate Rd', 'Northgate', 'Brisbane', 'QLD', '4025', 45, '2015-02-12', 'IronMan', -27.390002, 153.0733509),
(38, 'Justin', '58', 'Main St', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-16', 'Suresh', -27.4368363, 153.03108989999998),
(42, 'Kane', '16.0', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030.0', 45, '2015-08-27', 'Suresh', -27, 153),
(43, 'Undertaker', '16.0', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030.0', 45, '2015-08-27', 'Suresh', -27, 153),
(44, 'Randy', '16.0', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030.0', 45, '2015-08-27', 'Suresh', -27, 153),
(45, 'King', '15', 'Rose', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-19', 'IronMan', -27.4368363, 153.03108989999998),
(46, 'Queen', '22', 'King St', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-21', 'IronMan', -27.4368363, 153.03108989999998),
(47, 'ABCD', '18', 'Ann St', 'Brisbane CBD', 'Brisbane', 'QLD', '4030', 45, '2015-02-23', 'IronMan', -27.4710107, 153.02344889999995),
(54, 'Verdi', '16', 'Rosemount', 'Windsor', 'Brisbane', 'QLD', '4030', 45, '2015-02-28', 'Batman', -27.4368363, 153.03108989999998),
(55, 'Steyn', '16.0', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030.0', 45, '2015-08-27', 'Suresh', -27, 153),
(56, 'AB', '16.0', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030.0', 45, '2015-08-27', 'Suresh', -27, 153),
(57, 'Faf', '16.0', 'Rosemount Tce', 'Windsor', 'Brisbane', 'QLD', '4030.0', 45, '2015-08-27', 'Suresh', -27, 153);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `parent_role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `branch_id`, `parent_role_id`) VALUES
(11, 'user', 'Normal User', 39, NULL),
(12, 'admin', 'Administrator', 39, 11);

-- --------------------------------------------------------

--
-- Table structure for table `tenant`
--

CREATE TABLE IF NOT EXISTS `tenant` (
`id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tenant`
--

INSERT INTO `tenant` (`id`, `name`) VALUES
(28, 'iphealth');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `name` varchar(250) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `is_super_user` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `name`, `branch_id`, `is_super_user`, `password`) VALUES
(45, 'iphealth', 'IPAdmin', 39, 1, 'test'),
(46, 'suresh', 'Suresh', 39, 0, 'sachin'),
(47, 'ironman', 'IronMan', 39, 0, 'jarvis'),
(48, 'batman', 'Batman', 39, 0, 'wayne');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `visit_limits`
--

CREATE TABLE IF NOT EXISTS `visit_limits` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(15) NOT NULL,
  `date` varchar(11) NOT NULL,
  `limits` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visit_limits`
--

INSERT INTO `visit_limits` (`id`, `user_id`, `user_name`, `date`, `limits`) VALUES
(1, 45, 'IPAdmin', '2015-05-22', 3),
(2, 45, 'IPAdmin', '2015-02-19', 3),
(3, 45, 'IPAdmin', '2015-02-20', 2),
(4, 47, 'IronMan', '2015-02-22', 2),
(5, 47, 'IronMan', '2015-02-24', 2),
(6, 47, 'IronMan', '2015-02-28', 2),
(7, 45, 'IPAdmin', '2015-02-28', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address_details`
--
ALTER TABLE `address_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_name_UNIQUE` (`unique_name`), ADD KEY `fk_branch_branch1_idx` (`parent_branch_id`), ADD KEY `fk_branch_tenant1_idx` (`tenant_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `geo_fence`
--
ALTER TABLE `geo_fence`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_group_branch1_idx` (`branch_id`);

--
-- Indexes for table `inspection_details`
--
ALTER TABLE `inspection_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_messsage_user1_idx` (`user_id`);

--
-- Indexes for table `others_location`
--
ALTER TABLE `others_location`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_role_branch1_idx` (`branch_id`), ADD KEY `fk_role_role1_idx` (`parent_role_id`);

--
-- Indexes for table `tenant`
--
ALTER TABLE `tenant`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_user_branch1_idx` (`branch_id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
 ADD PRIMARY KEY (`user_id`,`group_id`), ADD KEY `fk_user_has_group_group1_idx` (`group_id`), ADD KEY `fk_user_has_group_user1_idx` (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
 ADD PRIMARY KEY (`user_id`,`role_id`), ADD KEY `fk_user_has_role_role1_idx` (`role_id`), ADD KEY `fk_user_has_role_user1_idx` (`user_id`);

--
-- Indexes for table `visit_limits`
--
ALTER TABLE `visit_limits`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address_details`
--
ALTER TABLE `address_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `geo_fence`
--
ALTER TABLE `geo_fence`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `inspection_details`
--
ALTER TABLE `inspection_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=140;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `others_location`
--
ALTER TABLE `others_location`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tenant`
--
ALTER TABLE `tenant`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `visit_limits`
--
ALTER TABLE `visit_limits`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `branch`
--
ALTER TABLE `branch`
ADD CONSTRAINT `fk_branch_branch1` FOREIGN KEY (`parent_branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_branch_tenant1` FOREIGN KEY (`tenant_id`) REFERENCES `tenant` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `group`
--
ALTER TABLE `group`
ADD CONSTRAINT `fk_group_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
ADD CONSTRAINT `fk_messsage_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `role`
--
ALTER TABLE `role`
ADD CONSTRAINT `fk_role_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_role_role1` FOREIGN KEY (`parent_role_id`) REFERENCES `role` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `fk_user_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_group`
--
ALTER TABLE `user_group`
ADD CONSTRAINT `fk_user_has_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_has_group_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
ADD CONSTRAINT `fk_user_has_role_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_user_has_role_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
