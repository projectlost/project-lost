App.register.controller('ViewModifyRolesController', function ($http, $scope, $route, $location, WebService, ModuleProvider, Popup, toaster) {
    $scope.module = {
        title: 'Manage Roles'
    };

    $scope.allRoles = [];

    $scope.listAllRoles = function () {

        WebService.invokePOSTRequest('user/viewAllRoles', {}, function (data) {
            $scope.allRoles = angular.fromJson(data);
        }, function (data) {
            toaster.pop('error', "Uh oh", "Could not retrieve roles");
        });
    };

    // remove user
    $scope.removeRole = function (data) {
        Popup.show('Delete Role', 'Deleting the role would delete all permissions assigned to that role. Are you sure?', 'yesno', 'medium')

		.then(function (result) {
		    if (result == 'yes') {
		        var param = {
		            'name': data.name
		        };

		        WebService.invokePOSTRequest('user/deleteARole', param, function (data) {
		            $scope.listAllRoles();
		            toaster.pop('success', "Success", "Successfully deleted role");
		        }, function (data, status, headers, config) {
		            toaster.pop('error', "Uh oh", "Could not delete role");
		        });
		    }
		});
    };

    $scope.saveRole = function (r, data) {

        // Create new role
        if (r.unsaved) {

            var param = {
                'name': data.name,
                'description': data.description || '',
                'parentRoleId': data.parent_role_id || ''
            };

            WebService.invokePOSTRequest('user/createARole', param, function (data) {
                $scope.listAllRoles();
                toaster.pop('success', "Success", "Successfully created role");
            }, function (data, status, headers, config) {
                toaster.pop('error', "Uh oh", "Could not create role");
            });
        }
            // Update role
        else {
            var param = {
                'id': r.id,
                'name': data.name,
                'description': data.description || '',
                'parentRoleId': data.parent_role_id || ''
            };

            WebService.invokePOSTRequest('user/updateARole', param, function (data, status, headers) {
                $scope.listAllRoles();
                toaster.pop('success', "Success", "Successfully updated role");
            }, function (data, status, headers, config) {
                toaster.pop('error', "Uh oh", "Could not update role");
            });
        }
    };

    $scope.addNewRole = function () {
        $scope.inserted = {
            name: null,
            description: null,
            parent_role_id: null,
            unsaved: true
        };
        $scope.allRoles.push($scope.inserted);
    };

    $scope.validateName = function (data) {
        if (!data || data.length <= 0) {
            return "Specify role name";
        }
    };

    $scope.cancel = function (rowform, data) {
        rowform.$cancel();
    };

    // All roles except current one
    $scope.getRolesToPickAsParent = function (rid) {
        var parentRoles = [null];
        angular.forEach($scope.allRoles, function (r) {
            if ((r.id != rid.id) && !r.unsaved) {
                parentRoles.push(r);
            }
        });

        return parentRoles;
    };

    $scope.getRoleNameById = function (rid) {
        var name = null;
        angular.forEach($scope.allRoles, function (r) {
            if (r.id == rid) {
                name = r.name;
            }
        });

        return name;
    };

    $scope.listAllRoles();
});