App.register.controller('AddUserToRoleAndGroupPopupController', function($scope, $http, $modalInstance, WebService, toaster, user) {

	$scope.user = user;
	$scope.radioModel = {
		radioModel : 'role'
	};

	$scope.rolesorgroups = [];

	$scope.$watch('radioModel.radioModel', function() {
		$scope.populateGroupsOrRolesPicker();
	});

	$scope.populateGroupsOrRolesPicker = function() {

		var serviceUrl = null;

		if ($scope.radioModel.radioModel == 'role') {
			serviceUrl = 'getUserRolesToAssign';
		} else if ($scope.radioModel.radioModel == 'group') {
			serviceUrl = 'getUserGroupsToAssign';
		}

		var params = {
			"uid" : $scope.user.id,
		};

		WebService.invokePOSTRequest('user/' + serviceUrl, params, function(data) {
			$scope.rolesorgroups = angular.fromJson(data);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not retrieve " + $scope.radioModel.radioModel + " to pick from");
		}, function() {
		});
	};

	$scope.assignOrUnassignRoleOrGroupToUser = function(rog, assignOrUnassign) {

		var serviceUrl = null;
		var errorMessage = null;
		var sucessMessage = null;

		if ($scope.radioModel.radioModel == 'role') {
			serviceUrl = 'assignUserToRole';
		} else if ($scope.radioModel.radioModel == 'group') {
			serviceUrl = 'assignUserToGroup';
		}

		if (assignOrUnassign) {
			errorMessage = 'assign user to ';
			sucessMessage = 'assigned user to that ';
		} else {
			errorMessage = 'unassign user from ';
			sucessMessage = 'unassigned user from that ';
		}

		var params = {
			"uid" : $scope.user.id,
			"rgid" : rog.id,
			"assignOrUnassign" : assignOrUnassign
		};

		WebService.invokePOSTRequest('user/' + serviceUrl, params, function(data) {
			$scope.populateGroupsOrRolesPicker();
			toaster.pop('success', "Success", "Successfully " + sucessMessage + $scope.radioModel.radioModel);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not " + errorMessage + $scope.radioModel.radioModel);
		}, function() {
		});
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});

App.register.controller('AddUserPopupController', function($scope, $http, $modalInstance, WebService, toaster, user) {

	$scope.user = {
		is_super_user : false
	};

	$scope.enablePassField = (user == null);
	$scope.user = user ? user : $scope.user;

	$scope.submitUser = function() {
		// user == null means the modal is invoked for create
		if (user == null) {
			$scope.createUser();
		} else {
			$scope.updateUser();
		}
	};

	$scope.updateUser = function() {
		if (!$scope.user.name || !$scope.user.id) {
			return;
		}

		var params = {
			"sAdmin" : $scope.user.is_super_user,
			"fName" : $scope.user.name,
			"fSurname" : $scope.user.surname,
			"fEmail" : $scope.user.email,
			"uuid" : $scope.user.id,
		};

		WebService.invokePOSTRequest('user/updateAUser', params, function(data) {
			$modalInstance.close();
			toaster.pop('success', "Successfully updated user", "Successfully updated the user");
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not update user");
		}, function() {
		});
	};

	$scope.createUser = function() {
		if (!$scope.user.name || !$scope.user.user_name || !$scope.user.pass) {
			return;
		}

		var params = {
			"sAdmin" : $scope.user.is_super_user,
			"fName" : $scope.user.name,
			"uName" : $scope.user.user_name,
			"fSurname" : $scope.user.surname,
			"fEmail" : $scope.user.email,
			"pass" : $scope.user.pass,
		};

		WebService.invokePOSTRequest('user/createAUser', params, function(data) {
			$modalInstance.close();
			toaster.pop('success', "Successfully created user", "Successfully created the user");
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not create user");
		}, function() {
		});
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});

App.register.controller('ViewAllUsersController', function($q, $filter, $modal, $http, $scope, $route, $location, WebService, Popup, toaster, ngTableParams) {
	$scope.module = {
		title : 'Manage Users'
	};

	$scope.users = [];

	$scope.deleteUser = function(user) {

		Popup.show('Delete User', 'Are you sure you want to delete ' + user.name + '?', 'yesno')

		.then(function(result) {
			if (result == 'yes') {
				var params = {
					"duid" : user.id
				};

				WebService.invokePOSTRequest('user/deleteAUser', params, function(data) {
					$scope.tableParams.reload();
					toaster.pop('success', "Successfully deleted user", "Successfully deleted the user");
				}, function(data) {
					toaster.pop('error', "Uh oh", "Could not delete user");
				}, function() {
				});
			}
		});
	};

	$scope.addUser = function() {
		$modal.open({
			templateUrl : 'modules/user/userformpopup.html',
			controller : 'AddUserPopupController',
			resolve : {
				user : function() {
					return null;
				}
			}
		}).result.then(function() {
			$scope.tableParams.reload();
		}, function() {
		});
	};

	$scope.getUsers = function(page, count) {
		var deferred = $q.defer();

		var params = {
			'pageNumber' : page,
			'pageCount' : count
		};

		WebService.invokePOSTRequest('user/viewAllUsers', params, function(data) {
			$scope.users = angular.fromJson(data);
			deferred.resolve($scope.users);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get users");
		});

		return deferred.promise;
	};

	$scope.updateDetails = function(user) {
		$modal.open({
			templateUrl : 'modules/user/userformpopup.html',
			controller : 'AddUserPopupController',
			resolve : {
				user : function() {
					// return copy to prevent original from getting modified
					return angular.copy(user);
				}
			}
		}).result.then(function() {
			$scope.tableParams.reload();
		}, function() {
		});
	};

	$scope.updatePermission = function(user) {
		$modal.open({
			templateUrl : 'modules/user/userassignrolegrouppopup.html',
			controller : 'AddUserToRoleAndGroupPopupController',
			resolve : {
				user : function() {
					return user;
				}
			}
		}).result.then(function() {
		}, function() {
		});
	};

	$scope.tableParams = new ngTableParams({
		page : 1,
		count : 10,
		sorting : {
		// name : 'asc'
		}
	}, {
		total : -1,
		getData : function($defer, params) {
			var promise = $scope.getUsers(params.page(), params.count());

			promise.then(function() {
				if ($scope.users.length > 0) {
					params.total($scope.users[0].totalrecords);
				}

				$scope.users = params.sorting() ? $filter('orderBy')($scope.users, params.orderBy()) : $scope.users;
				$defer.resolve($scope.users);
			});
		}
	});
});