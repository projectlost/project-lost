App.register.controller('MasterController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster, Popup) {

	$scope.module = {
		title : 'Master Schedule',
		addfeature : 'Master Schedule - Add Feature',
	};
	
	var date;
	$scope.property = [];
	$scope.insDetails1 = [];
	$scope.insDetails2 = [];
	$scope.insDetails3 = [];
	$scope.visitLimits = [];
	$scope.final_add = [];
	var ins_dates;
	$scope.all_ins = [];
	var newDate,currDate,brk_cnt;
	var count = 0;
	var distance;
	var dist_array = [];

	var myLatLng;
	var t_date = new Date();
	brk_cnt = 0;
	
	localStorage.removeItem("today_date");
	//localStorage.removeItem("todays_date");
	localStorage.removeItem("tday_date");
	
	local_t_date = new Date(localStorage.getItem("todays_date"));
	var t_date_String = t_date.toDateString();
	var local_t_date_String = local_t_date.toDateString();
	if(t_date_String != local_t_date_String) {
		localStorage.setItem("final_arrayLength", 0);
	}
	
	/*
	 * Invokes a function from the controller to upload data from an excel sheet.
	 */
	$scope.readProp = function() {
		
		WebService.invokePOSTRequest('masterschedule/loadFromFile', {}, function(data) {
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not save the property");
		});
	};
	
	/*
	 * Gets visit limit value from the user and store it in the database.
	 */
	$scope.saveLimits = function() {
		
		var lmt = parseInt($scope.param.limits);
		
		var params = {
				"date" : $scope.param.date,
				"limits" : lmt
		};
		
		WebService.invokePOSTRequest('masterschedule/saveLimits', params, function(data) {
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not save the visit limits");
		});
		
	};
	
	/*
	 * Gets today's date.
	 * Change it to string format.
	 * Get Latitude and Longitude value of the address.
	 * Save the address and the date in the database.
	 */
	$scope.saveProp = function() {
		
		date = new Date();
		var dd = date.getDate();
		var mm = date.getMonth() + 1;
		var yyyy = date.getFullYear();

		if(dd < 10) {
		    dd = '0' + dd;
		} 

		if(mm < 10) {
		    mm = '0' + mm;
		} 

		date = yyyy + '-' + mm + '-' + dd;
		
		var usrBox = document.getElementById("user");
		var usr = usrBox.options[usrBox.selectedIndex].text;
		
		geocoder = new google.maps.Geocoder(); 
		var address = $scope.param.suburb + ", " + $scope.param.state;
		
		geocoder.geocode( { 'address': address}, function(results, status){
			if (status == google.maps.GeocoderStatus.OK) {
				myLatLng = results[0].geometry.location;
			}
			else {
				alert("Geocode was not successful for the following reason: " + status);
			}
			
			var params = {
					"name" : $scope.param.name,
					"unit": $scope.param.unit,
					"street" : $scope.param.street,
					"suburb" : $scope.param.suburb,
					"city" : $scope.param.city,
					"state" : $scope.param.state,
					"postcode" : $scope.param.postcode,
					"date" : date,
					"usr" : usr,
					"lat" : myLatLng.k,
					"lng" : myLatLng.D
				};
			
			WebService.invokePOSTRequest('masterschedule/saveProperty', params, function(data) {
				$scope.listProperty();
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not save the property");
			});
			
		});
		
		
	};
	
	/*
	 * Gets the property value from the database
	 */
	$scope.listProperty = function() {
		
		WebService.invokePOSTRequest('masterschedule/listProperty', {}, function(data) {
			$scope.property = angular.fromJson(data);
			$scope.addInsDates();
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get properties");
		});
	};
	
	/*
	 * Gets the property value from the database
	 */
	$scope.listAllProperty = function() {
		
		WebService.invokePOSTRequest('masterschedule/listProperty', {}, function(data) {
			$scope.all_prop = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get properties");
		});
	};
	
	/*
	 * Gets the inspection details from the database based on the property id
	 */
	$scope.allIns = function(id) {
		
		var params = {
				"id" : id
		};
		WebService.invokePOSTRequest('masterschedule/listAllIns', params, function(data) {
			$scope.all_ins = angular.fromJson(data);
			$scope.dispIns($scope.all_ins);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get properties");
		});
		
	};
	
	/*
	 * Display a popup with the inspection dates
	 */
	$scope.dispIns = function(insArray) {
		ins_dates = '';
		for(var i = 0; i < insArray.length; i++) {
			ins_dates = ins_dates + insArray[i].date;
			if(i != (insArray.length - 1)) {
				ins_dates = ins_dates + ', ';
			}
		}
		
		Popup.show('Dates','The inspection dates for the selected property are \n' + ins_dates);
	};
	
	/*
	 * Calculate inspection dates based on the state.
	 * If the date falls on Saturday or Sunday, change it to the following Monday.
	 * Save it in the database.
	 */
	$scope.addInsDates = function() {
		
		if($scope.param.state == "QLD" || $scope.param.state == "NSW" || $scope.param.state == "VIC" || 
				$scope.param.state == "SA" || $scope.param.state == "WA" || $scope.param.state == "TAS"
					|| $scope.param.state == "NT" || $scope.param.state == "NZ") {
			currDate = new Date(date);
			newDate = new Date();
			do {
				newDate.setDate(currDate.getDate() + 91);
				
				var weekd = newDate.getDay();
				if(weekd == 7) {
					newDate.setDate(newDate.getDate() + 1);
				}
				else if(weekd == 6) {
					newDate.setDate(newDate.getDate() + 1);
				}
				var day = newDate.getDate();
				var month = newDate.getMonth()+1;
				var year = newDate.getFullYear();
			
				if(day < 10) {
				    day = '0' + day;
				} 

				if(month < 10) {
				    month = '0' + month;
				} 

				ndate = year + '-' + month + '-' + day;
				
				var params = {
						"pid" : $scope.property[$scope.property.length - 1].id,
						"date" : ndate
					};
				if(currDate.getFullYear() >= newDate.getFullYear()) {
					WebService.invokePOSTRequest('masterschedule/saveInspection', params, function(data) {							
					}, function(data, status, headers, config) {
						toaster.pop('error', "Uh oh", "Could not save the inspection dates");
					});
				}			
			}while(currDate.getFullYear() >= newDate.getFullYear());
		}
		else if($scope.param.state == "ACT") {
			currDate = new Date(date);
			newDate = new Date();
			do {
				newDate.setDate(currDate.getDate() + 182);
				
				var weekd = newDate.getDay();
				if(weekd == 7) {
					newDate.setDate(newDate.getDate() + 1);
				}
				else if(weekd == 6) {
					newDate.setDate(newDate.getDate() + 1);
				}
				var day = newDate.getDate();
				var month = newDate.getMonth()+1;
				var year = newDate.getFullYear();
			
				if(day < 10) {
				    day = '0' + day;
				} 

				if(month < 10) {
				    month = '0' + month;
				} 

				ndate = year + '-' + month + '-' + day;
				
				var params = {
						"pid" : $scope.property[$scope.property.length - 1].id,
						"date" : ndate
					};
				if(currDate.getFullYear() >= newDate.getFullYear()) {
					WebService.invokePOSTRequest('masterschedule/saveInspection', params, function(data) {							
					}, function(data, status, headers, config) {
						toaster.pop('error', "Uh oh", "Could not save the inspection dates");
					});
				}			
			}while(currDate.getFullYear() >= newDate.getFullYear());
		}
		$scope.param.name = "";
		$scope.param.unit = "";
		$scope.param.street = "";
		$scope.param.suburb = "";
		$scope.param.city = "";
		$scope.param.state = "";
		$scope.param.postcode = "";
		
	};
	/*
	 * Retrive 3 sets of inspection details.
	 * 1st set with today's date.
	 * 2nd set with yesterday's date.
	 * 3rd set with tomorrow's date.
	 */
	$scope.InsDetails = function() {
		
		date = new Date();
		var dd = date.getDate();
		var mm = date.getMonth() + 1;
		var yyyy = date.getFullYear();

		if(dd < 10) {
		    dd = '0' + dd;
		} 

		if(mm < 10) {
		    mm = '0' + mm;
		} 

		date = yyyy + '-' + mm + '-' + dd;
		
		var params = {
				"date" : date
			};
		
			WebService.invokePOSTRequest('masterschedule/retInspectionStat', params, function(data) {
				$scope.insDetails2 = angular.fromJson(data);
				count++;
				if(count == 3) {
					$scope.optimizeResults();
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get details");
			});
			
			WebService.invokePOSTRequest('masterschedule/retLimits', params, function(data) {
				$scope.visitLimits = angular.fromJson(data);
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get details");
			});
		
		var yday_Date = new Date(date);
		yday_Date.setDate(yday_Date.getDate() - 1);
		
		var y_dd = yday_Date.getDate();
		var y_mm = yday_Date.getMonth() + 1;
		var y_yyyy = yday_Date.getFullYear();

		if(y_dd < 10) {
			y_dd = '0' + y_dd;
		} 

		if(y_mm < 10) {
			y_mm = '0' + y_mm;
		} 

		var y_date = y_yyyy + '-' + y_mm + '-' + y_dd;
		
		var y_param = {
				"date" : y_date
		};
		
			WebService.invokePOSTRequest('masterschedule/retInspectionStat', y_param, function(data) {
				$scope.insDetails1 = angular.fromJson(data);
				count++;
				if(count == 3) {
					$scope.optimizeResults();
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get details");
			});
		
		
		var sec_Date = new Date(date);
		sec_Date.setDate(sec_Date.getDate() + 1);
		
		var sec_dd = sec_Date.getDate();
		var sec_mm = sec_Date.getMonth() + 1;
		var sec_yyyy = sec_Date.getFullYear();

		if(sec_dd < 10) {
			sec_dd = '0' + sec_dd;
		} 

		if(sec_mm < 10) {
			sec_mm = '0' + sec_mm;
		} 

		var sec_date = sec_yyyy + '-' + sec_mm + '-' + sec_dd;
		
		var sec_param = {
				"date" : sec_date
		};
		
			WebService.invokePOSTRequest('masterschedule/retInspectionStat', sec_param, function(data) {
				$scope.insDetails3 = angular.fromJson(data);
				count++;
				if(count == 3) {
					$scope.optimizeResults();
				}
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get details");
			});

	};
	/*
	 * With the 3 sets of inspection details optimize the locations based on the similarity of the suburbs
	 * then with the distance between suburbs and then limit the results according to the user's limit details
	 * for that particular day.
	 */
	$scope.optimizeResults = function() {
		
		var j = 0;
		if(localStorage.getItem("final_arrayLength") == 0) {
			if($scope.insDetails1.length == 0 && $scope.insDetails3.length == 0) {
				for(var i = 0; i < $scope.visitLimits[0].limts; i++) {
					$scope.final_add[i] = $scope.insDetails2[i];
				}
			}
			else if($scope.insDetails1.length != 0 && $scope.insDetails2.length != 0) {
				var s_urb = $scope.insDetails1[0].subUrb;
				var s_lat = $scope.insDetails2[0].lat;
				var s_lng = $scope.insDetails2[0].lng;
				for(var i = 0; i < $scope.insDetails1.length; i++) {
					if(s_urb == $scope.insDetails1[i].subUrb) {
						if($scope.final_add.length == $scope.visitLimits[0].limts) {
							break;
						}
						else if($scope.final_add.length == 4) {
							break;
						}
						$scope.final_add[j] = $scope.insDetails1[i];
						j++;
					}
				}
				for (var k = 0; k < $scope.insDetails2.length; k++) {
					if(s_urb == $scope.insDetails2[k].subUrb) {
						if($scope.final_add.length == $scope.visitLimits[0].limts) {
							break;
						}
						else if($scope.final_add.length == 4) {
							break;
						}
						$scope.final_add[j] = $scope.insDetails2[k];
						j++;
					}
				}
				if($scope.final_add.length < 4) {
					for(var i = 0; i < $scope.insDetails1.length; i++) {
						if(s_urb != $scope.insDetails1[i].subUrb) {
							distance = $scope.calcDistance(s_lat, s_lng, $scope.insDetails1[i].lat, $scope.insDetails1[i].lng);
							dist_array.push({
								dist: distance,
								id: $scope.insDetails1[i].id
							});
						}
					}
					for (var k = 0; k < $scope.insDetails2.length; k++) {
						if(s_urb != $scope.insDetails2[k].subUrb) {
							distance = $scope.calcDistance(s_lat, s_lng, $scope.insDetails2[k].lat, $scope.insDetails2[k].lng);
							dist_array.push({
								dist: distance,
								id: $scope.insDetails2[k].id
							});
						}
					}
					for(var a = 0; a < dist_array.length - 1; a++) {
						if (dist_array[a].dist > dist_array[a + 1].dist) {
			                var temp = dist_array[a];
			                dist_array[a] = dist_array[a + 1];
			                dist_array[a + 1] = temp;
			            }
					}
					for(var b = 0; b < dist_array.length; b++) {
						for(var c = 0; c < $scope.insDetails1.length; c++) {
							if(dist_array[b].id == $scope.insDetails1[c].id) {
								if(j == $scope.visitLimits[0].limts) {
									break;
									brk_cnt = 1;
								}
								else if(j == 4) {
									break;
									brk_cnt = 1;
								}
								$scope.final_add[j] = $scope.insDetails1[c];
								j++;
								
							}
						}
						if(brk_cnt == 1) {
							break;
						}
						for(var d = 0; d < $scope.insDetails2.length; d++) {
							if(dist_array[b].id == $scope.insDetails2[d].id) {
								if(j == $scope.visitLimits[0].limts) {
									break;
									brk_cnt = 1;
								}
								else if(j == 4) {
									break;
									brk_cnt = 1;
								}
								$scope.final_add[j] = $scope.insDetails2[d];
								j++;
								
							}
						}
						if(brk_cnt == 1) {
							break;
						}
					}
				}
				for(var m = 0; m < $scope.final_add.length; m++) {
					var params  = {
							"id" : $scope.final_add[m].id
						};
						WebService.invokePOSTRequest('masterschedule/updInspection', params, function(data) {							
						}, function(data, status, headers, config) {
							toaster.pop('error', "Uh oh", "Could not update the inspection dates");
						});
				}
				localStorage.setItem("todays_date", new Date());
			}
			else if($scope.insDetails2.length != 0 && $scope.insDetails3.length != 0) {
				var s_urb = $scope.insDetails2[0].subUrb;
				var s_lat = $scope.insDetails2[0].lat;
				var s_lng = $scope.insDetails2[0].lng;
				for(var i = 0; i < $scope.insDetails2.length; i++) {
					if(s_urb == $scope.insDetails2[i].subUrb) {
						$scope.final_add[j] = $scope.insDetails2[i];
						j++;
					}
				}
				for (var k = 0; k < $scope.insDetails3.length; k++) {
					if(s_urb == $scope.insDetails3[k].subUrb) {
						if($scope.final_add.length == 4) {
							break;
						}
						$scope.final_add[j] = $scope.insDetails3[k];
						j++;
					}
				}
				if($scope.final_add.length < 4) {
					for(var i = 0; i < $scope.insDetails2.length; i++) {
						if(s_urb != $scope.insDetails2[i].subUrb) {
							distance = $scope.calcDistance(s_lat, s_lng, $scope.insDetails2[i].lat, $scope.insDetails2[i].lng);
							dist_array.push({
								dist: distance,
								id: $scope.insDetails2[i].id
							});
						}
					}
					for (var k = 0; k < $scope.insDetails3.length; k++) {
						if(s_urb != $scope.insDetails3[k].subUrb) {
							distance = $scope.calcDistance(s_lat, s_lng, $scope.insDetails3[k].lat, $scope.insDetails3[k].lng);
							dist_array.push({
								dist: distance,
								id: $scope.insDetails3[k].id
							});
						}
					}
					for(var a = 0; a < dist_array.length - 1; a++) {
						if (dist_array[a].dist > dist_array[a + 1].dist) {
			                var temp = dist_array[a];
			                dist_array[a] = dist_array[a + 1];
			                dist_array[a + 1] = temp;
			            }
					}
					for(var b = 0; b < dist_array.length; b++) {
						for(var c = 0; c < $scope.insDetails2.length; c++) {
							if(dist_array[b].id == $scope.insDetails2[c].id) {
								if(j == 4) {
									break;
									brk_cnt = 1;
								}
								$scope.final_add[j] = $scope.insDetails2[c];
								j++;
								
							}
						}
						if(brk_cnt == 1) {
							break;
						}
						for(var d = 0; d < $scope.insDetails3.length; d++) {
							if(dist_array[b].id == $scope.insDetails3[d].id) {
								if(j == 4) {
									break;
									brk_cnt = 1;
								}
								$scope.final_add[j] = $scope.insDetails3[d];
								j++;
								
							}
						}
						if(brk_cnt == 1) {
							break;
						}
					}
				}
				
				
				
				
				for(var m = 0; m < $scope.final_add.length; m++) {
					var params  = {
							"id" : $scope.final_add[m].id
						};
						WebService.invokePOSTRequest('masterschedule/updInspection', params, function(data) {							
						}, function(data, status, headers, config) {
							toaster.pop('error', "Uh oh", "Could not update the inspection dates");
						});
				}
				
				localStorage.setItem("todays_date", new Date());
				
			}
			localStorage["final_array"] = JSON.stringify($scope.final_add);
			
			localStorage.setItem("final_arrayLength", $scope.final_add.length);
		}
		else {
			$scope.final_add = JSON.parse(localStorage["final_array"]);
		}
		
		
	};
	/*
	 * Calculate distance between two suburbs.
	 */
	$scope.calcDistance = function(lat1, lng1, lat2, lng2) {
		
		var R = 6378137;
	    var dLat = (lat2 - lat1)* (Math.PI / 180);
	    var dLong = (lng2 - lng1)* (Math.PI / 180);
	    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	      		Math.cos(lat2 * (Math.PI / 180)) * Math.cos(lat1 * (Math.PI / 180)) *
	      		Math.sin(dLong / 2) * Math.sin(dLong / 2);
	    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    var distance = R * c;
		
	    return distance;
		
	
	};
	
	/*
	 * Create new text boxes in the HTML page.
	 * Display the address in the text boxes.
	 */
	
	$scope.dispAddress = function() {
		
		var div = document.getElementById('multAdd');
		while(div.firstChild){
		    div.removeChild(div.firstChild);
		}
		
		var seldiv = document.getElementById('cusOrderDiv');
		while(seldiv.firstChild){
		    seldiv.removeChild(seldiv.firstChild);
		}
		
		for(var i = 0; i < $scope.final_add.length; i++) {
			var element = document.createElement("input");
			
			element.setAttribute("type", "text");
			element.setAttribute("class","form-control");
			element.setAttribute("value", $scope.final_add[i].unit + " " + $scope.final_add[i].street + " " + $scope.final_add[i].subUrb + " " + $scope.final_add[i].city);
			element.setAttribute("id", "dispLoc" + (i + 1));
			element.setAttribute("placeholder","Search location to navigate");

			var divId = document.getElementById("multAdd");
			divId.appendChild(element);
			
			var selElement = document.createElement("select");
			selElement.setAttribute("id", "custOrder" + (i + 1));
			selElement.setAttribute("class","form-control");
			selElement.setAttribute("hidden","true");
			
			var custDiv = document.getElementById("cusOrderDiv");
			custDiv.appendChild(selElement);
			
			var selTag = document.getElementById("custOrder" + (i + 1));
			selTag.style.visibility = 'hidden';
			
			
		}
		
	};
	
	/*
	 * Add the addresses to the drop down list.
	 */
	$scope.showCustomAddress = function() {
		var i;
		var j;
		for(i = 1; i <= $scope.insDetails.length; i++) {
			var sel = document.getElementById("custOrder" + i);
			sel.style.visibility = 'visible';
			for(j = 1; j <= $scope.insDetails.length; j++) {
				var ad_loc = document.getElementById("dispLoc" + j);
				var opt = document.createElement("option");
				opt.value = j;
				opt.text = ad_loc.value;
				sel.appendChild(opt);
			}
		}
	};
	
	$scope.listAllProperty();
	$scope.InsDetails();
	
	
	$scope.param = $scope.params || $scope.param;
});

App.register.controller('ViewAllUsersController', function($q, $filter, $modal, $http, $scope, $route, $location, WebService, Popup, toaster, ngTableParams) {

	$scope.users = [];
	/*
	 * Gets all registered users.
	 */
	$scope.getUsers = function(page, count) {
		
		var params = {
				'pageNumber' : page,
				'pageCount' : count
			};

		WebService.invokePOSTRequest('user/viewAllUsers', params, function(data) {
			$scope.users = angular.fromJson(data);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get users");
		});

	};
	
	$scope.getUsers(1,10);
});

var lat,lng;
App.register.directive("myMap", function() {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs ) {
			
			/*
			 * Gets the current location of the device(laptop or mobile)
			 */
			$scope.getCurrent = function() {
				navigator.geolocation.getCurrentPosition(function(position) {
				      lat = position.coords.latitude;
				      lng = position.coords.longitude;
				      
				    });	
				
			};
			
			/*
			 * Decodes the text into latitude and longitude values.
			 * Show the route between your current location and the destination.
			 */
			$scope.navigate = function(unit, street, suburb, city) {

				//$scope.param['$scope'] = $scope;
				
				var directionsDisplay;
				var directionsService = new google.maps.DirectionsService();
				var map;
				var myLatLng;
				
				
				
				directionsDisplay = new google.maps.DirectionsRenderer();
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				var address = unit + " " + street + " " + suburb + " " + city;
			    geocoder.geocode( { 'address': address}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			            map.setCenter(results[0].geometry.location);
			            myLatLng = results[0].geometry.location;
			        }
			        else
			        {
			            alert("Geocode was not successful for the following reason: " + status);
			        }
					  
					  map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
					  directionsDisplay.setMap(map);
					  directionsDisplay.setPanel(document.getElementById('directionsPanel'));
					  
					  google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
						   
						  });

					  
				            var start = new google.maps.LatLng(lat,lng);
							  
							  var request = {
							      origin:start,
							      destination:myLatLng,
							      travelMode: google.maps.TravelMode.DRIVING
							  };
							  directionsService.route(request, function(response, status) {
							    if (status == google.maps.DirectionsStatus.OK) {
							      directionsDisplay.setDirections(response);
							    }
							  });
					  
					  
					  
					  
			    });
			    
			    
				  
				
			};
			
			/*
			 * Use the locations from the text boxes as way points.
			 * Display the route.
			 * Rearrange the waypoints based on the user's custom order.
			 * 
			 */
			$scope.showRoute = function() {
				
				var directionsDisplay;
				var directionsService = new google.maps.DirectionsService();
				var i,j,k,l,m,loc,count,loc_order,custSel,custVal,map,myLatLng;
				var nameTxt = [];
				var waypts = [];
				var wayptsOrder = [];
        		var custWayPts = [];
				
        		l = 1;
				
				directionsDisplay = new google.maps.DirectionsRenderer();
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				for(i = 1; i <= $scope.insDetails.length; i++) {
					var address = document.getElementById("dispLoc" + i).value;
					
					geocoder.geocode( { 'address': address}, function(results, status){
						if (status == google.maps.GeocoderStatus.OK) {
							nameTxt[l] = results[0].formatted_address;
							l++;
							map.setCenter(results[0].geometry.location);
							myLatLng = results[0].geometry.location;
						}
						else {
							alert("Geocode was not successful for the following reason: " + status);
						}
						
						waypts.push({
							location: myLatLng,
							stopover: true
						});
						
						if(i == (waypts.length + 1)) {
							for(k = 1; k <= $scope.insDetails.length; k++) {
								for(m = 1; m < l; m++) {
									if((document.getElementById("dispLoc" + k).value).substr(0,4) == nameTxt[m].substr(0,4)) {
										loc_order = new google.maps.LatLng(waypts[m - 1].location.k,waypts[m - 1].location.D);
										wayptsOrder.push({
											location: loc_order,
											stopover: true
										});
									}
								}

							}
						}
						
						 map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
						 directionsDisplay.setMap(map);
						 directionsDisplay.setPanel(document.getElementById('directionsPanel'));
						
						var start = new google.maps.LatLng(lat,lng);
						if((i == (wayptsOrder.length + 1)) && (document.getElementById("RevBox").checked)) {
							wayptsOrder.reverse();
			            }
						if((i == (wayptsOrder.length + 1)) && (document.getElementById("CustomBox").checked)) {
			            	for(j = 1;j <= $scope.userGrp.length;j++) {
			            		var custSel = document.getElementById("custOrder" + j);
			            		var custVal = custSel.options[custSel.selectedIndex].value;
			            		loc = new google.maps.LatLng(wayptsOrder[custVal - 1].location.k,wayptsOrder[custVal - 1].location.D);
			            		custWayPts.push({
			            			location: loc,
			            			stopover: true
			            		});
			            	}
			            	count = 1;
			            }
						if(!document.getElementById("DestBox").checked) {
							var stop = new google.maps.LatLng(waypts[waypts.length - 1].location.k,waypts[waypts.length - 1].location.D);
							if((i == (wayptsOrder.length + 1)) && (document.getElementById("CustomBox").checked)) {
								var stop = new google.maps.LatLng(custWayPts[custWayPts.length - 1].location.k,custWayPts[custWayPts.length - 1].location.D);
								custWayPts.pop();
							}
							
							if((i == (wayptsOrder.length + 1)) && !(document.getElementById("CustomBox").checked)) {
								var stop = new google.maps.LatLng(wayptsOrder[wayptsOrder.length - 1].location.k,wayptsOrder[wayptsOrder.length - 1].location.D);
								wayptsOrder.pop();
							}	
						}
						else {
							stop = start;
						}
						
						if(document.getElementById("TollBox").checked && count == 1) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: custWayPts,
								      optimizeWaypoints: false,
								      avoidTolls: true,
								      travelMode: google.maps.TravelMode.DRIVING
								};
						}
						else if(count == 1) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: custWayPts,
								      optimizeWaypoints: false,
								      travelMode: google.maps.TravelMode.DRIVING
								};
						}
						else if(document.getElementById("TollBox").checked) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: wayptsOrder,
								      optimizeWaypoints: false,
								      avoidTolls: true,
								      travelMode: google.maps.TravelMode.DRIVING
								};
							
						}
						else {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: wayptsOrder,
								      optimizeWaypoints: false,
								      travelMode: google.maps.TravelMode.DRIVING
								};
							
						}
												
						directionsService.route(request, function(response, status) {
						    if (status == google.maps.DirectionsStatus.OK) {
						      directionsDisplay.setDirections(response);
						    }
						});
						
					});
				}
				
			};
		}
	};
	
});