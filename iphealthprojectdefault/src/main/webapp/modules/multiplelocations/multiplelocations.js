var idIndex;
var idCount;
App.register.controller('MultipleLocationsController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Navigation',
		addfeature : 'Navigation - Add Feature',
	};
	idIndex = 1;
	$scope.users = [];
	
	/*
	 * Adds a new textbox in the HTML page
	 * Creates a dropdown list and adds it to the HTML page
	 */
	$scope.addTextBox = function(){
		var element = document.createElement("input");

		element.setAttribute("type", "text");
		element.setAttribute("class","form-control");
		element.setAttribute("value", "");
		element.setAttribute("id", "addLoc" + idIndex);
		element.setAttribute("placeholder","Search location to navigate");

		var divId = document.getElementById("multLoc");
		divId.appendChild(element);
		
		var selElement = document.createElement("select");
		selElement.setAttribute("id", "customOrder" + idIndex);
		selElement.setAttribute("class","form-control");
		selElement.setAttribute("hidden","true");
		
		var custDiv = document.getElementById("cusDiv");
		custDiv.appendChild(selElement);
		
		var selTag = document.getElementById("customOrder" + idIndex);
		selTag.style.visibility = 'hidden';
		
		idIndex++;
		
	};
	
	/*
	 * Gets the value from the textboxes and adds the value into the dropdown list
	 */
	$scope.showCustom = function() {
		var i;
		var j;
		for(i = 1; i < idIndex; i++) {
			var sel = document.getElementById("customOrder" + i);
			sel.style.visibility = 'visible';
			for(j = 1; j < idIndex; j++) {
				var ad_loc = document.getElementById("addLoc" + j);
				var opt = document.createElement("option");
				opt.value = j;
				opt.text = ad_loc.value;
				sel.appendChild(opt);
			}
		}
	};
	
	$scope.param = $scope.params || $scope.param;
});

App.register.controller('ProvideLocationsController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Navigation',
		addfeature : 'Navigation - Add Feature',
	};
	idCount = 1;
	$scope.userLoc = [];
	
	/*
	 * Adds a new textbox in the HTML page
	 */
	$scope.addBox = function(){
		var element = document.createElement("input");

		element.setAttribute("type", "text");
		element.setAttribute("class","form-control");
		element.setAttribute("value", "");
		element.setAttribute("id", "proLoc" + idCount);
		element.setAttribute("placeholder","Provide Address");

		var divId = document.getElementById("prodLoc");
		divId.appendChild(element);
		
		idCount++;
		
	};
	/*
	 * Gets the values entered into the textbox.
	 * Invokes a function from the controller to save the value into the database.
	 */
	$scope.provideLoc = function() {
		
		var dropBox = document.getElementById("userDetails");
		var user = dropBox.options[dropBox.selectedIndex].text;
		var grpNo = document.getElementById("groupNo").value;
		for(var i = 1; i < idCount; i++) {
			var box = document.getElementById("proLoc" + i).value;
			
			var params = {
					"address" : box,
					"user": user,
					"grp" : grpNo
				};
			
			WebService.invokePOSTRequest('multiplelocations/saveAddress', params, function(data) {
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not save the address");
			});
		}
		
		var div = document.getElementById('prodLoc');
		while(div.firstChild){
		    div.removeChild(div.firstChild);
		}
		
		
	};
	
	/*
	 * Invokes a function from the controller to list the user locations.
	 */
	$scope.retUserLoc = function() {
		
		WebService.invokePOSTRequest('multiplelocations/listUserLocation', {}, function(data) {
			$scope.userLoc = angular.fromJson(data);
			//$scope.dispAddress();
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get the details");
		});
		
	};
	/*
	 * Gets user details based on the group.
	 */
	$scope.getDetails = function() {
		
		var grpBox = document.getElementById("grpDetails");
		var grp = grpBox.options[grpBox.selectedIndex].text;
		
		
		var params = {
				"grp" : grp
			};
		
		WebService.invokePOSTRequest('multiplelocations/listAddDetails', params, function(data) {
			$scope.userGrp = angular.fromJson(data);
			$scope.dispAddress();
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get the details");
		});
	};
	
	/*
	 * Create new text boxes.
	 * Display the retrieved results.
	 * 
	 */
	$scope.dispAddress = function() {
		
		var div = document.getElementById('prodUserLoc');
		while(div.firstChild){
		    div.removeChild(div.firstChild);
		}
		
		var seldiv = document.getElementById('cusOrderDiv');
		while(seldiv.firstChild){
		    seldiv.removeChild(seldiv.firstChild);
		}
		
		for(var i = 0; i < $scope.userGrp.length; i++) {
			var element = document.createElement("input");

			element.setAttribute("type", "text");
			element.setAttribute("class","form-control");
			element.setAttribute("value", $scope.userGrp[i].address);
			element.setAttribute("id", "dispLoc" + (i + 1));
			element.setAttribute("placeholder","Search location to navigate");

			var divId = document.getElementById("prodUserLoc");
			divId.appendChild(element);
			
			var selElement = document.createElement("select");
			selElement.setAttribute("id", "custOrder" + (i + 1));
			selElement.setAttribute("class","form-control");
			selElement.setAttribute("hidden","true");
			
			var custDiv = document.getElementById("cusOrderDiv");
			custDiv.appendChild(selElement);
			
			var selTag = document.getElementById("custOrder" + (i + 1));
			selTag.style.visibility = 'hidden';
			
			
		}
		
	};
	
	/*
	 * Display the drop down list with the options.
	 */
	$scope.showCustomAddress = function() {
		var i;
		var j;
		for(i = 1; i <= $scope.userGrp.length; i++) {
			var sel = document.getElementById("custOrder" + i);
			sel.style.visibility = 'visible';
			for(j = 1; j <= $scope.userGrp.length; j++) {
				var ad_loc = document.getElementById("dispLoc" + j);
				var opt = document.createElement("option");
				opt.value = j;
				opt.text = ad_loc.value;
				sel.appendChild(opt);
			}
		}
	};
	
	$scope.retUserLoc();
	
	
	$scope.param = $scope.params || $scope.param;
});

App.register.controller('ViewAllUsersController', function($q, $filter, $modal, $http, $scope, $route, $location, WebService, Popup, toaster, ngTableParams) {

	$scope.users = [];
	/*
	 * Get all the registered users
	 */
	$scope.getUsers = function(page, count) {
		
		var params = {
				'pageNumber' : page,
				'pageCount' : count
			};

		WebService.invokePOSTRequest('user/viewAllUsers', params, function(data) {
			$scope.users = angular.fromJson(data);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get users");
		});

	};
	
	$scope.getUsers(1,10);
});

var lat;
var lng;
App.register.directive("myDir", function() {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs ) {
			
			/*
			 * Gets the current location of the device(laptop or mobile)
			 */
			$scope.getCurrent = function() {
				navigator.geolocation.getCurrentPosition(function(position) {
				      lat = position.coords.latitude;
				      lng = position.coords.longitude;
				      
				    });	
				
			};
			
			/*
			 * Use the locations from the text boxes as way points.
			 * Display the route.
			 * Rearrange the waypoints based on the user's custom order.
			 * 
			 */
			$scope.showRoute = function() {
				
				var directionsDisplay;
				var directionsService = new google.maps.DirectionsService();
				var i,j,k,l,m,loc,count,loc_order,custSel,custVal,map,myLatLng;
				var nameTxt = [];
				var waypts = [];
				var wayptsOrder = [];
        		var custWayPts = [];
				
        		l = 1;
				
				directionsDisplay = new google.maps.DirectionsRenderer();
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				for(i = 1; i < idIndex; i++) {
					var address = document.getElementById("addLoc" + i).value;
					
					geocoder.geocode( { 'address': address}, function(results, status){
						if (status == google.maps.GeocoderStatus.OK) {
							nameTxt[l] = results[0].formatted_address;
							l++;
							map.setCenter(results[0].geometry.location);
							myLatLng = results[0].geometry.location;
						}
						else {
							alert("Geocode was not successful for the following reason: " + status);
						}
						
						waypts.push({
							location: myLatLng,
							stopover: true
						});
						
						if(i == (waypts.length + 1)) {
							for(k = 1; k < idIndex; k++) {
								for(m = 1; m < l; m++) {
									if((document.getElementById("addLoc" + k).value).substr(0,4) == nameTxt[m].substr(0,4)) {
										loc_order = new google.maps.LatLng(waypts[m - 1].location.k,waypts[m - 1].location.D);
										wayptsOrder.push({
											location: loc_order,
											stopover: true
										});
									}
								}

							}
						}
						
						 map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
						 directionsDisplay.setMap(map);
						 directionsDisplay.setPanel(document.getElementById('directionsPanel'));
						
						var start = new google.maps.LatLng(lat,lng);
						if((i == (wayptsOrder.length + 1)) && (document.getElementById("chkBox").checked)) {
							wayptsOrder.reverse();
			            }
						if((i == (wayptsOrder.length + 1)) && (document.getElementById("chkCustomBox").checked)) {
			            	for(j = 1;j < idIndex;j++) {
			            		var custSel = document.getElementById("customOrder" + j);
			            		var custVal = custSel.options[custSel.selectedIndex].value;
			            		loc = new google.maps.LatLng(wayptsOrder[custVal - 1].location.k,wayptsOrder[custVal - 1].location.D);
			            		custWayPts.push({
			            			location: loc,
			            			stopover: true
			            		});
			            	}
			            	count = 1;
			            }
						if(!document.getElementById("chkDestBox").checked) {
							var stop = new google.maps.LatLng(waypts[waypts.length - 1].location.k,waypts[waypts.length - 1].location.D);
							if((i == (wayptsOrder.length + 1)) && (document.getElementById("chkCustomBox").checked)) {
								var stop = new google.maps.LatLng(custWayPts[custWayPts.length - 1].location.k,custWayPts[custWayPts.length - 1].location.D);
								custWayPts.pop();
							}
							
							if((i == (wayptsOrder.length + 1)) && !(document.getElementById("chkCustomBox").checked)) {
								var stop = new google.maps.LatLng(wayptsOrder[wayptsOrder.length - 1].location.k,wayptsOrder[wayptsOrder.length - 1].location.D);
								wayptsOrder.pop();
							}	
						}
						else {
							stop = start;
						}
						
						if(document.getElementById("chkTollBox").checked && count == 1) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: custWayPts,
								      optimizeWaypoints: false,
								      avoidTolls: true,
								      travelMode: google.maps.TravelMode.DRIVING
								};
						}
						else if(count == 1) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: custWayPts,
								      optimizeWaypoints: false,
								      travelMode: google.maps.TravelMode.DRIVING
								};
						}
						else if(document.getElementById("chkTollBox").checked) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: wayptsOrder,
								      optimizeWaypoints: false,
								      avoidTolls: true,
								      travelMode: google.maps.TravelMode.DRIVING
								};
							
						}
						else {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: wayptsOrder,
								      optimizeWaypoints: false,
								      travelMode: google.maps.TravelMode.DRIVING
								};
							
						}
												
						directionsService.route(request, function(response, status) {
						    if (status == google.maps.DirectionsStatus.OK) {
						      directionsDisplay.setDirections(response);
						    }
						});
						
					});
				}
				
			};
			
			/*
			 * Use the locations from the text boxes as way points.
			 * Display the route.
			 * Rearrange the waypoints based on the user's custom order.
			 * 
			 */
			$scope.showUserRoute = function() {
				
				var directionsDisplay;
				var directionsService = new google.maps.DirectionsService();
				var i,j,k,l,m,loc,count,loc_order,custSel,custVal,map,myLatLng;
				var nameTxt = [];
				var waypts = [];
				var wayptsOrder = [];
        		var custWayPts = [];
				
        		l = 1;
				
				directionsDisplay = new google.maps.DirectionsRenderer();
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				for(i = 1; i <= $scope.userGrp.length; i++) {
					var address = document.getElementById("dispLoc" + i).value;
					
					geocoder.geocode( { 'address': address}, function(results, status){
						if (status == google.maps.GeocoderStatus.OK) {
							nameTxt[l] = results[0].formatted_address;
							l++;
							map.setCenter(results[0].geometry.location);
							myLatLng = results[0].geometry.location;
						}
						else {
							alert("Geocode was not successful for the following reason: " + status);
						}
						
						waypts.push({
							location: myLatLng,
							stopover: true
						});
						
						if(i == (waypts.length + 1)) {
							for(k = 1; k <= $scope.userGrp.length; k++) {
								for(m = 1; m < l; m++) {
									if((document.getElementById("dispLoc" + k).value).substr(0,4) == nameTxt[m].substr(0,4)) {
										loc_order = new google.maps.LatLng(waypts[m - 1].location.k,waypts[m - 1].location.D);
										wayptsOrder.push({
											location: loc_order,
											stopover: true
										});
									}
								}

							}
						}
						
						 map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
						 directionsDisplay.setMap(map);
						 directionsDisplay.setPanel(document.getElementById('directionsPanel'));
						
						var start = new google.maps.LatLng(lat,lng);
						if((i == (wayptsOrder.length + 1)) && (document.getElementById("RevBox").checked)) {
							wayptsOrder.reverse();
			            }
						if((i == (wayptsOrder.length + 1)) && (document.getElementById("CustomBox").checked)) {
			            	for(j = 1;j <= $scope.userGrp.length;j++) {
			            		var custSel = document.getElementById("custOrder" + j);
			            		var custVal = custSel.options[custSel.selectedIndex].value;
			            		loc = new google.maps.LatLng(wayptsOrder[custVal - 1].location.k,wayptsOrder[custVal - 1].location.D);
			            		custWayPts.push({
			            			location: loc,
			            			stopover: true
			            		});
			            	}
			            	count = 1;
			            }
						if(!document.getElementById("DestBox").checked) {
							var stop = new google.maps.LatLng(waypts[waypts.length - 1].location.k,waypts[waypts.length - 1].location.D);
							if((i == (wayptsOrder.length + 1)) && (document.getElementById("CustomBox").checked)) {
								var stop = new google.maps.LatLng(custWayPts[custWayPts.length - 1].location.k,custWayPts[custWayPts.length - 1].location.D);
								custWayPts.pop();
							}
							
							if((i == (wayptsOrder.length + 1)) && !(document.getElementById("CustomBox").checked)) {
								var stop = new google.maps.LatLng(wayptsOrder[wayptsOrder.length - 1].location.k,wayptsOrder[wayptsOrder.length - 1].location.D);
								wayptsOrder.pop();
							}	
						}
						else {
							stop = start;
						}
						
						if(document.getElementById("TollBox").checked && count == 1) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: custWayPts,
								      optimizeWaypoints: false,
								      avoidTolls: true,
								      travelMode: google.maps.TravelMode.DRIVING
								};
						}
						else if(count == 1) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: custWayPts,
								      optimizeWaypoints: false,
								      travelMode: google.maps.TravelMode.DRIVING
								};
						}
						else if(document.getElementById("TollBox").checked) {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: wayptsOrder,
								      optimizeWaypoints: false,
								      avoidTolls: true,
								      travelMode: google.maps.TravelMode.DRIVING
								};
							
						}
						else {
							var request = {
								      origin:start,
								      destination:stop,
								      waypoints: wayptsOrder,
								      optimizeWaypoints: false,
								      travelMode: google.maps.TravelMode.DRIVING
								};
							
						}
												
						directionsService.route(request, function(response, status) {
						    if (status == google.maps.DirectionsStatus.OK) {
						      directionsDisplay.setDirections(response);
						    }
						});
						
					});
				}
				
			};
		}
	};
	
});