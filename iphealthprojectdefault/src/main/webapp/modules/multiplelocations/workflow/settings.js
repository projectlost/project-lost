App.register.controller('MultipleLocationsSettingsController', function($scope, WebService, toaster) {

	/*
	 * $scope.data.feature gets attached to the scope by the system when a
	 * settings dialog is invoked.
	 */

	/*
	 * Gets parameters that are already present or initializes a default one
	 */
	$scope.param = $scope.data.feature.params ? $scope.data.feature.params : {
		'srcLat' : 0,
		'srcLng' : 0,
		'destLat' : 0,
		'destLng' : 0
	};

	/*
	 * Gets called by the system when 'apply' button is clicked
	 */
	$scope.data.ok = function() {
		var srcLat = parseInt($scope.param.srcLat);
		var srcLng = parseInt($scope.param.srcLng);
		var destLat = parseInt($scope.param.destLat);
		var destLng = parseInt($scope.param.destLng);

		$scope.param.srcLat = srcLat;
		$scope.param.srcLat = srcLng;
		$scope.param.destLat = destLat;
		$scope.param.destLng = destLng;

		/*
		 * Returns the json object parameters that has to be pass to the
		 * associated feature UI
		 */
		return $scope.param;

		/* Return 'cancel' if settings form validation fails. */
	};

	/*
	 * Sets the title to display
	 */
	$scope.data.title = 'Multiple Navigation';

	/*
	 * $scope.data.previewparams is passed to the associated UI when the
	 * settings dialogue is invoked as part of workflow creator
	 * 
	 * NOTE: ignore for the moment
	 */
	$scope.data.previewparams = $scope.param;
});