var u_lat;
var u_lng;
App.register.controller('OthersLocationController', function($q, $rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Others Location',
		addfeature : 'Location - Add Feature',
	};
	
	$scope.otherloc = [];
	$scope.specificLoc = [];	
	
	/*
	 * Invokes a function from the controller to get all the data from the database
	 */
	$scope.getOthersLoc = function() {
		
		WebService.invokePOSTRequest('otherslocation/listLocation', {}, function(data) {
			$scope.otherLoc = angular.fromJson(data);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get location");
		});
		
	};
	/*
	 * Invokes a function from the controller to get the data from the database
	 */
	$scope.getSpecificLoc = function() {
		
		WebService.invokePOSTRequest('otherslocation/listSpecLocation', {}, function(data) {
			$scope.specificLoc = angular.fromJson(data);
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get location");
		});
		
	};

	$scope.getOthersLoc();
	$scope.getSpecificLoc();
	
	$scope.param = $scope.params || $scope.param;
});

App.register.directive("myFence", function(WebService, toaster) {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs ) {
			
			/*
			 * Gets the current location of the device(laptop or mobile)
			 */
			$scope.getCurrentUserLoc = function() {
				navigator.geolocation.getCurrentPosition(function(position) {
					u_lat = position.coords.latitude;
					u_lng = position.coords.longitude;
				    });
				
				setInterval($scope.getCurrentUserLoc, 100000);
			};
			
			/*
			 * Save the current user's location and the profile picture into the database
			 */
			$scope.storeUserLoc = function() {
				
				/*navigator.geolocation.getCurrentPosition(function(position) {
					u_lat = position.coords.latitude;
					u_lng = position.coords.longitude;
				    });*/
				
				var url = document.getElementById("url").innerHTML;
				
				var url1 = url.toString();
				var userCenter = new google.maps.LatLng(u_lat, u_lng);
				
				var mapOptions = {
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP 
					};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
			    map.setCenter(userCenter);
			    
			    var marker = new google.maps.Marker({
			                map: map,
			                position: userCenter
			            });
			    
			    var params = {
						"lat" : u_lat,
						"lng" : u_lng,
						"url" : url1
					};
				
				WebService.invokePOSTRequest('otherslocation/saveLocation', params, function(data) {
					$scope.getOthersLoc();
				}, function(data, status, headers, config) {
					toaster.pop('error', "Uh oh", "Could not store the location");
				});
				
				setInterval($scope.storeUserLoc, 100000);
				
			};
			
			/*
			 * Get the selected user and his location.
			 * Display it on the map.
			 * Change the default icon on the map to the display picture of the selected user.
			 */
			$scope.showUserLoc = function() {
				
				var count = 0;
				var i = 0;
				var pic;
				var userCenter;
				var chk = document.getElementById("chkBox");
				if(!chk.checked) {
					var e = document.getElementById("otherDetails");
					var name = e.options[e.selectedIndex].text;
				}
				else {
					var e = document.getElementById("specDetails");
					var name = e.options[e.selectedIndex].text;
				}
				
				for(i = 0; i < $scope.otherLoc.length; i++) {
					if(name == $scope.otherLoc[i].name)
					{
						userCenter = new google.maps.LatLng($scope.otherLoc[i].lat, $scope.otherLoc[i].lng);
						pic = $scope.otherLoc[i].url;
						break;
					}
				}
				
				var mapOptions = {
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP 
					};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
			    map.setCenter(userCenter);
			    
			    var image = {
			    	    url: pic,
			    	    scaledSize: new google.maps.Size(26, 25),
			    	    origin: new google.maps.Point(0,0),
			    	    anchor: new google.maps.Point(13, 30)
			    	  };
			    
			    var img = {
			    		url: 'core/img/marker.png',
			    		scaledSize: new google.maps.Size(32, 32)
			    };
			    
			    var marker1 = new google.maps.Marker({
	                map: map,
	                position: userCenter,
	                icon: img
	            });
			    
			    var marker = new google.maps.Marker({
			                map: map,
			                position: userCenter,
			                icon: image
			            });
			    
			    
				
			};
		
		}
	};
	
});