App.register.controller('MapPlacementController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Map Placement',
		addfeature : 'Map Placement - Add Feature',
	};
	/*
	 * $scope.params is the instance of parameter object created using the
	 * settings dialog
	 */
	$scope.param = $scope.params || $scope.param;
});
var park_add;
var park_loc;
App.register.directive("myMaps", function(WebService, toaster) {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs) {
			
			/*
			 * Decodes the text into latitude and longitude values.
			 * Shows the location on the map.
			 * Enable street view on the map.
			 */
			
			$scope.showStreetView = function() {

				/*
				 * Needed for workflow (NOTE: Ignore for now)
				 */
				$scope.param['$scope'] = $scope;
				geocoder = new google.maps.Geocoder();
				
				var myLatLng;
				var address = document.getElementById("address").value;
			    geocoder.geocode( { 'address': address}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			        	myLatLng = results[0].geometry.location;
			        }
			        else
			        {
			            alert("Geocode was not successful for the following reason: " + status);
			        }
			        
			        var panoramaOptions = {
						    position: myLatLng,
						    pov: {
						      heading: 34,
						      pitch: 10
						    }
						  };

					var map = new google.maps.Map(document.getElementById(attrs.id), panoramaOptions);
					var panorama = new google.maps.StreetViewPanorama(document.getElementById(attrs.id), panoramaOptions);
			        
					map.setStreetView(panorama);
			    });		
			    
			    WebService.invokePOSTRequest('mapplacement/showLocation', $scope.param, function(data) {
					//$scope.param = angular.fromJson(data);
				}, function(data, status, headers, config) {
					toaster.pop('error', "Uh oh", "Could not show map");
				});
			};
			
			/*
			 * Decodes the text into latitude and longitude values.
			 * Shows the location on the map.
			 */
			$scope.showLocation = function() {

				/*
				 * Needed for workflow (NOTE: Ignore for now)
				 */
				$scope.param['$scope'] = $scope;
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				var address = document.getElementById("address").value;
			    geocoder.geocode( { 'address': address}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			            map.setCenter(results[0].geometry.location);
			            park_add = results[0].formatted_address;
			            park_loc = results[0].geometry.location;
			            var marker = new google.maps.Marker(
			            {
			                map: map,
			                position: results[0].geometry.location
			            });
			        }
			        else
			        {
			            alert("Geocode was not successful for the following reason: " + status);
			        }
			    });
			    
			    WebService.invokePOSTRequest('mapplacement/showLocation', $scope.param, function(data) {
					//$scope.param = angular.fromJson(data);
				}, function(data, status, headers, config) {
					toaster.pop('error', "Uh oh", "Could not show Street view");
				});
			    
				
			};
			
			$scope.showParking = function() {
				
				var mapOptions = {
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP 
					};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				var marker = new google.maps.Marker({
					map: map,
					position: park_loc
				});
				var img = {
					url: 'core/img/parking.png',
			    	scaledSize: new google.maps.Size(32, 32)		
			    };
				park_add = "parking lots near " + park_add;
				var request = {
						location: park_loc,
					    radius: '500',
					    query: park_add
				};
					    
				var service = new google.maps.places.PlacesService(map);
				service.textSearch(request, function(results, status) {
					if (status == google.maps.places.PlacesServiceStatus.OK) {
						for (var i = 0; i < results.length; i++) {
							var place = results[i];
							var marker = new google.maps.Marker({
							    map: map,
							    position: results[i].geometry.location,
							    icon: img
							  });
						}
					}
					
				});		
				map.setCenter(park_loc);
				
			};
		}
	};
	
});