App.register.controller('MapPlacementSettingsController', function($scope, WebService, toaster) {

	/*
	 * $scope.data.feature gets attached to the scope by the system when a
	 * settings dialog is invoked.
	 */

	/*
	 * Gets parameters that are already present or initializes a default one
	 */
	$scope.param = $scope.data.feature.params ? $scope.data.feature.params : {
		'Latitude' : 0,
		'Longitde' : 0
	};

	/*
	 * Gets called by the system when 'apply' button is clicked
	 */
	$scope.data.ok = function() {
		var Latitude = parseInt($scope.param.Latitude);
		var Longitde = parseInt($scope.param.Longitde);

		$scope.param.Latitude = Latitude;
		$scope.param.Longitde = Longitde;

		/*
		 * Returns the json object parameters that has to be pass to the
		 * associated feature UI
		 */
		return $scope.param;

		/* Return 'cancel' if settings form validation fails. */
	};

	/*
	 * Sets the title to display
	 */
	$scope.data.title = 'Show Map';

	/*
	 * $scope.data.previewparams is passed to the associated UI when the
	 * settings dialogue is invoked as part of workflow creator
	 * 
	 * NOTE: ignore for the moment
	 */
	$scope.data.previewparams = $scope.param;
});