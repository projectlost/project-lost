App.register.controller('FenceAlertSettingsController', function($scope, WebService, toaster) {

	$scope.data.title = 'Fence Alert';

	/*
	 * $scope.data.previewparams is passed to the associated UI when the
	 * settings dialogue is invoked as part of workflow creator
	 * 
	 * NOTE: ignore for the moment
	 */
	$scope.data.previewparams = $scope.param;
});