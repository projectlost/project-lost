var u_lat;
var u_lng;
App.register.controller('FenceAlertController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Fence Alert',
		addfeature : 'Alert - Add Feature',
	};
	
	/*
	 * Invokes listFences method of the controller and gets the data from the database
	 */
	$scope.getAllFences = function() {
		WebService.invokePOSTRequest('geofencing/listFences', {}, function(data) {
			$scope.allfences = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get fences");
		});
	};
	
	$scope.getAllFences();	
	
	$scope.param = $scope.params || $scope.param;
});
/*
 * Directive to display the google map
 */
App.register.directive("myFence", function(WebService, toaster) {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs ) {
			/*
			 * Gets the current location of the device(laptop or mobile)
			 */
			$scope.getCurrentLoc = function() {
				navigator.geolocation.getCurrentPosition(function(position) {
					u_lat = position.coords.latitude;
					u_lng = position.coords.longitude;
				      
				    });
				setInterval($scope.getCurrentLoc, 100000); /* Invokes the same function after 100000 secs*/
			};
			
			/*
			 * Calculates the distance between the fence center and the current location.
			 * If the distance is greater than the fence radius then alert will be triggered.
			 * This function will be called every 120000 secs.
			 */
			$scope.alertFence = function() {
				
				var count = 0;
				var i;
				var userCenter = new google.maps.LatLng(u_lat, u_lng);
				
				var fenceCircle;
				var fenceCenter;
				
				var mapOptions = {
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP 
					};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
			    map.setCenter(userCenter);
			    
			    var marker = new google.maps.Marker({
			                map: map,
			                position: userCenter
			            });
			    for(i = 0; i < $scope.allfences.length; i++) {
			    	var R = 6378137;
				    var dLat = ($scope.allfences[i].lat - u_lat)* (Math.PI / 180);
				    var dLong = ($scope.allfences[i].lng - u_lng)* (Math.PI / 180);
				    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				      		Math.cos($scope.allfences[i].lat * (Math.PI / 180)) * Math.cos(u_lat * (Math.PI / 180)) *
				      		Math.sin(dLong / 2) * Math.sin(dLong / 2);
				    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
				    var d = R * c;
				    
				    if(d < $scope.allfences[i].radius) {
				    	count = 1;
				    	toaster.pop('success', "Inside", "User is inside the " + $scope.allfences[i].name + " fence!!");
				    	break;
			    	}
			    	
			    }
			    
			    if(count == 0) {
			    	toaster.pop('success', "Outside", "User is outside the fences !!");
		    	}
			   
			    
			    setInterval($scope.alertFence, 120000);
				
			};
		
		}
	};
	
});