App.register.controller('GeoFencingController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Geofencing',
		addfeature : 'Geofencing - Add Feature',
	};

	/*
	 * Invokes listFences from the controller.
	 * Gets values from the database.
	 */
	$scope.listAllFences = function() {
		WebService.invokePOSTRequest('geofencing/listFences', {}, function(data) {
			$scope.fences = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get fences");
		});
	};
	
	$scope.listAllFences();
	
	
	$scope.param = $scope.params || $scope.param;
});

App.register.directive("myFence", function(WebService, toaster) {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs ) {
			
			/*
			 * Decodes the text into latitude and longitude values.
			 * Calculates radius.
			 * Draws a circle in the map with the calculated values.
			 * Saves it in the database.
			 */
			$scope.createFence = function() {

				/*
				 * Needed for workflow (NOTE: Ignore for now)
				 */
				$scope.param['$scope'] = $scope;
				
				var fenceCenter;
				var fenceCircle;
				var lt;
				var lg;
				var rd;
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				var fence = document.getElementById("fence").value;
			    geocoder.geocode( { 'address': fence}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			            map.setCenter(results[0].geometry.location);
			            fenceCenter = results[0].geometry.location;
			            lt = fenceCenter.k;
			            lg = fenceCenter.D;
			            var marker = new google.maps.Marker(
			            {
			                map: map,
			                position: results[0].geometry.location
			            });
			            if(document.getElementById("miles").checked) {
			            	rd = parseFloat($scope.param.radius) * 1600;
			            }
			            else if(document.getElementById("kms").checked) {
			            	rd = parseFloat($scope.param.radius) * 1000;
			            }
			            else if(document.getElementById("meters").checked) {
			            	rd = parseFloat($scope.param.radius);
			            }
			            var fenceOptions = {
					    	      strokeColor: '#FF0000',
					    	      strokeOpacity: 0.8,
					    	      strokeWeight: 2,
					    	      fillColor: '#FF0000',
					    	      fillOpacity: 0.35,
					    	      map: map,
					    	      center: fenceCenter,
					    	      radius: rd
					    	    };
					    	    // Add the circle for this city to the map.
					    fenceCircle = new google.maps.Circle(fenceOptions);
					    var params = {
								"location" : $scope.param.fence,
								"radius": rd,
								"name" : $scope.param.name,
								"lat" : lt,
								"lng" : lg
							};
					    
					    WebService.invokePOSTRequest('geofencing/drawFence', params, function(data) {
					    	$scope.listAllFences();
						}, function(data, status, headers, config) {
							toaster.pop('error', "Uh oh", "Could not create a fence");
						});
			        }
			        else
			        {
			            alert("Geocode was not successful for the following reason: " + status);
			        }
			    });				
			};
			
			$scope.showFence = function(lat, lng, rad) {
				
				var fenceCircle;
				var fenceCenter;
				
				var mapOptions = {
						zoom: 15,
						mapTypeId: google.maps.MapTypeId.ROADMAP 
					};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				fenceCenter = new google.maps.LatLng(lat, lng);
				var fenceOptions = {
			    	      strokeColor: '#FF0000',
			    	      strokeOpacity: 0.8,
			    	      strokeWeight: 2,
			    	      fillColor: '#FF0000',
			    	      fillOpacity: 0.35,
			    	      map: map,
			    	      center: fenceCenter,
			    	      radius: parseFloat(rad)
			    	    };
			    	    
			    fenceCircle = new google.maps.Circle(fenceOptions);
			    map.setCenter(fenceCenter);
				
			};
		
		}
	};
	
});