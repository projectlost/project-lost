App.register.controller('GeoFencingSettingsController', function($scope, WebService, toaster) {

	/*
	 * $scope.data.feature gets attached to the scope by the system when a
	 * settings dialog is invoked.
	 */

	/*
	 * Gets parameters that are already present or initializes a default one
	 */
	$scope.param = $scope.data.feature.params ? $scope.data.feature.params : {
		'fence' : 0,
		'radius' : 0
	};

	/*
	 * Gets called by the system when 'apply' button is clicked
	 */
	$scope.data.ok = function() {
		var fence = parseInt($scope.param.fence);
		var radius = parseInt($scope.param.radius);

		$scope.param.fence = fence;
		$scope.param.radius = radius;

		/*
		 * Returns the json object parameters that has to be pass to the
		 * associated feature UI
		 */
		return $scope.param;

		/* Return 'cancel' if settings form validation fails. */
	};

	/*
	 * Sets the title to display
	 */
	$scope.data.title = 'Create Fence';

	/*
	 * $scope.data.previewparams is passed to the associated UI when the
	 * settings dialogue is invoked as part of workflow creator
	 * 
	 * NOTE: ignore for the moment
	 */
	$scope.data.previewparams = $scope.param;
});