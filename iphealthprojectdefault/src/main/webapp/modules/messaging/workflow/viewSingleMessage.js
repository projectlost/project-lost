App.register.controller('SingleMessageMessagingController', function($rootScope, $http, $scope, $route, $location, toaster, WebService, ModuleProvider) {
	$scope.messageId = $scope.params.id;
	$scope.message = 'Loading..';

	$scope.loadMessage = function() {
		if ($scope.messageId) {

			/*
			 * All web service call to the backend must be made through the
			 * 'WebService' AngularJs service.
			 * 
			 * The first parameter is the feature URL in the form of <module
			 * id>/<feature id>
			 * 
			 * The second parameter is a json object with request parameters
			 * where each parameter name maps the the @RequestParam value name
			 * in the backend controller base interface
			 * 
			 * Example: @RequestParam(value = "id", required = true) String id
			 * 
			 * The third parameter is the function to be called when the web
			 * service call is returned
			 * 
			 * The last parameter is the function to be called when the web
			 * service call returns an error
			 */

			var params = {
				"id" : $scope.messageId
			};

			WebService.invokePOSTRequest('messaging/getSingleMessage', params, function(data) {
				$scope.message = angular.fromJson(data).message;
			}, function(data, status, headers, config) {
				toaster.pop('error', "Uh oh", "Could not get message");
			});
		}
	};

	$scope.loadMessage();
});