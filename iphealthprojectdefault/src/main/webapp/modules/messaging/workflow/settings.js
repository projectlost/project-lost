App.register.controller('ViewSingleMessageSettingsController', function($scope, WebService, toaster) {

	$scope.param = $scope.data.feature.params ? $scope.data.feature.params : {
		'id' : 1
	};

	$scope.data.ok = function() {
		return $scope.param;
	};

	$scope.data.title = 'Pick the message to display';
	$scope.data.previewparams = $scope.param;
});