/*
 * All dynamically loaded controllers MUST be registered using App.register.controller
 */
App.register.directive('messageDirective', function(WebService, toaster) {
	return {
		restrict : 'E',
		scope : {
			message : '=',
			msgid : '=',
			deletecallback : '='
		},
		controller : function($scope) {
			$scope.deleteMessage = function(id) {

				var params = {
					"msgID" : id
				};

				WebService.invokePOSTRequest('messaging/delete', params, $scope.deletecallback, function(data, status, headers, config) {
					toaster.pop('error', "Uh oh", "Could not delete message");
				});
			};
		},
		templateUrl : 'modules/messaging/messagePartial.html'
	};
});

App.register.controller('MessagingController', function($rootScope, $http, $scope, $route, $location, toaster, safeApply, WebService, ChannelService, ModuleProvider) {
	$scope.module = {
		title : 'Messaging'
	};

	$scope.message = {
		message : ""
	};

	$scope.messages = [];

	$scope.sendMessage = function() {
		if (!$scope.message.message || $scope.message.message == '') {
			return;
		}

		var params = {
			"message" : $scope.message.message
		};

		WebService.invokePOSTRequest('messaging/send', params, function(data) {
			$scope.message.message = "";
			$scope.listAllMessages();
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not send message");
		});
	};

	$scope.listAllMessages = function() {
		WebService.invokePOSTRequest('messaging/list', {}, function(data) {
			$scope.messages = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not get messages");
		});
	};

	$scope.$on(ChannelService.messageReceivedEvent, function(evt, message) {
		if (message.id == 'x_newmessage') {
			safeApply($scope, function() {
				$scope.messages.push(angular.fromJson(message.message));
			});
		} else if (message.id == 'x_delete_message') {

			console.log('called');

			for (var index = 0; index < $scope.messages.length; index++) {
				if ($scope.messages[index].id == message.message) {
					safeApply($scope, function() {
						$scope.messages.splice(index, 1);
					});
					break;
				}

			}
		}
	});

	$scope.listAllMessages();
});