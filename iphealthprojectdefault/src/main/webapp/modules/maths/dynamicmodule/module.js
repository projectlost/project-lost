angular.module('SearchModuleBase', [])

.controller('SearchModuleBaseController', function($scope) {
	$scope.message = 'Loaded from module base';
});

angular.module('trunkmodule.maths', [ 'SearchModuleBase', 'SearchModuleBase3rd' ])

.controller('SearchModuleController', function($scope) {
	$scope.message = 'Loaded from module';
});