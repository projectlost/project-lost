App.register.controller('MathsSettingsController', function($scope, WebService, toaster) {

	/*
	 * $scope.data.feature gets attached to the scope by the system when a
	 * settings dialog is invoked.
	 */

	/*
	 * Gets parameters that are already present or initializes a default one
	 */
	$scope.param = $scope.data.feature.params ? $scope.data.feature.params : {
		'num1' : 0,
		'num2' : 0,
		'sum' : 0
	};

	/*
	 * Gets called by the system when 'apply' button is clicked
	 */
	$scope.data.ok = function() {
		var num1 = parseInt($scope.param.num1);
		var num2 = parseInt($scope.param.num2);

		$scope.param.num1 = num1;
		$scope.param.num2 = num2;
		$scope.param.sum = (num1 + num2);

		/*
		 * Returns the json object parameters that has to be pass to the
		 * associated feature UI
		 */
		return $scope.param;

		/* Return 'cancel' if settings form validation fails. */
	};

	/*
	 * Sets the title to display
	 */
	$scope.data.title = 'Select numbers';

	/*
	 * $scope.data.previewparams is passed to the associated UI when the
	 * settings dialogue is invoked as part of workflow creator
	 * 
	 * NOTE: ignore for the moment
	 */
	$scope.data.previewparams = $scope.param;
});