/*
 * All dynamically loaded controllers MUST be registered using App.register.controller
 */
App.register.controller('MathsController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Maths Module',
		addfeature : 'Math Module - Add Feature',
	};

	/*
	 * Initialize default parameters
	 */
	$scope.param = {
		'num1' : 0,
		'num2' : 0,
		'sum' : 0
	};

	$scope.add = function() {

		/*
		 * Needed for workflow (NOTE: Ignore for now)
		 */
		$scope.param['$scope'] = $scope;

		WebService.invokePOSTRequest('maths/add', $scope.param, function(data) {
			$scope.param = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not add numbers");
		});
	};

	$scope.subtract = function() {

		/*
		 * Needed for workflow (NOTE: Ignore for now)
		 */
		$scope.param['$scope'] = $scope;

		WebService.invokePOSTRequest('maths/subtract', $scope.param, function(data) {
			$scope.param = angular.fromJson(data);
		}, function(data, status, headers, config) {
			toaster.pop('error', "Uh oh", "Could not subtract numbers");
		});
	};

	/*
	 * $scope.params is the instance of parameter object created using the
	 * settings dialog
	 */
	$scope.param = $scope.params || $scope.param;
});