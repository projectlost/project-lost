App.register.controller('NavigationController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Navigation',
		addfeature : 'Navigation - Add Feature',
	};

	/*
	 * Initialize default parameters
	 */
	$scope.param = {
		'srcLat' : -27.477138,
		'srcLng' : 153.028436,
		'destLat' : 0,
		'destLng' : 0
	};
	/*
	 * $scope.params is the instance of parameter object created using the
	 * settings dialog
	 */
	
	$scope.param = $scope.params || $scope.param;
});
var lat;
var lng;
App.register.directive("myDir", function() {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs ) {
			
			/*
			 * Gets the current location of the device(laptop or mobile)
			 */
			$scope.getCurrent = function() {
				navigator.geolocation.getCurrentPosition(function(position) {
				      lat = position.coords.latitude;
				      lng = position.coords.longitude;
				      
				    });	
				
			};
			
			/*
			 * Decodes the text into latitude and longitude values.
			 * Show the route between your current location and the destination.
			 */
			$scope.showNavigation = function() {

				/*
				 * Needed for workflow (NOTE: Ignore for now)
				 */
				$scope.param['$scope'] = $scope;
				
				var directionsDisplay;
				var directionsService = new google.maps.DirectionsService();
				var map;
				var myLatLng;
				
				
				
				directionsDisplay = new google.maps.DirectionsRenderer();
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				var address = document.getElementById("address").value;
			    geocoder.geocode( { 'address': address}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			            map.setCenter(results[0].geometry.location);
			            myLatLng = results[0].geometry.location;
			        }
			        else
			        {
			            alert("Geocode was not successful for the following reason: " + status);
			        }
					  
					  map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
					  directionsDisplay.setMap(map);
					  directionsDisplay.setPanel(document.getElementById('directionsPanel'));
					  
					  google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
						   
						  });

					  
				            var start = new google.maps.LatLng(lat,lng);
							  
							  var request = {
							      origin:start,
							      destination:myLatLng,
							      travelMode: google.maps.TravelMode.DRIVING
							  };
							  directionsService.route(request, function(response, status) {
							    if (status == google.maps.DirectionsStatus.OK) {
							      directionsDisplay.setDirections(response);
							    }
							  });
					  
					  
					  
					  
			    });
			    
			    
				  
				
			};
		}
	};
	
});