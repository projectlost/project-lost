var lat;
var lng;
App.register.controller('NavigationController', function($rootScope, $http, $scope, $route, $location, ModuleProvider, WebService, toaster) {

	$scope.module = {
		title : 'Navigation',
		addfeature : 'Navigation - Add Feature',
	};
	
	$scope.param = $scope.params || $scope.param;
});

App.register.directive("myDir", function(WebService, toaster) {
	return {
		restrict:'E',
		template:'<div></div>',
		replace:true,
		link:function($scope, element, attrs ) {
			
			$scope.getCurrent = function() {
				navigator.geolocation.getCurrentPosition(function(position) {
					lat = position.coords.latitude;
					lng = position.coords.longitude;
				      
				    });
				setInterval($scope.getCurrent, 15000);
			};
			
			$scope.showNavigation = function() {

				/*
				 * Needed for workflow (NOTE: Ignore for now)
				 */
				$scope.param['$scope'] = $scope;
				
				var directionsDisplay;
				var directionsService = new google.maps.DirectionsService();
				var map;
				var myLatLng;
				
				
				
				directionsDisplay = new google.maps.DirectionsRenderer();
				
				geocoder = new google.maps.Geocoder(); 
				
				var mapOptions = {
					zoom: 15,
					mapTypeId: google.maps.MapTypeId.ROADMAP 
				};
				var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
				
				var address = document.getElementById("address").value;
			    geocoder.geocode( { 'address': address}, function(results, status)
			    {
			        if (status == google.maps.GeocoderStatus.OK)
			        {
			            map.setCenter(results[0].geometry.location);
			            var marker = new google.maps.Marker(
			            {
			                map: map,
			                position: results[0].geometry.location
			            });
			            myLatLng = results[0].geometry.location;
			        }
			        else
			        {
			            alert("Geocode was not successful for the following reason: " + status);
			        }
					  
					  map = new google.maps.Map(document.getElementById(attrs.id), mapOptions);
					  directionsDisplay.setMap(map);
				            
				            var start = new google.maps.LatLng(lat,lng);
							  
							  var request = {
							      origin:start,
							      destination:myLatLng,
							      travelMode: google.maps.TravelMode.DRIVING
							  };
							  directionsService.route(request, function(response, status) {
							    if (status == google.maps.DirectionsStatus.OK) {
							      directionsDisplay.setDirections(response);
							    }
							  });  
			    });		
			    
			    setInterval($scope.showNavigation, 20000);
			   
			};
		
		}
	};
	
});