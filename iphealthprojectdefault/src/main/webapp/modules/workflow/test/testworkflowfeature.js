App.register.controller('TestWorkflowFeatureController', function($q, $routeParams, $rootScope, $location, $scope, $modal, $timeout, FeatureSettings, Keyboard, Session, safeApply, toaster, Popup, WebService, ScriptLoader, Workflow) {

	/*
	 * Change to module id and feature id of the features you want to test out
	 */
	$scope.moduleid = 'maths';
	$scope.featureid = 'add';

	$scope.data = {
		'feature' : null,
		'previewparams' : {},
		'title' : 'Edit settings',
		'ok' : function() {
			return 'cancel';
		}
	};

	$scope.showPreview = false;

	$scope.viewtemplate = null;
	$scope.settingstemplate = null;

	$scope.loadFeatureSettings = function() {

		var params = {
			"mid" : $scope.moduleid,
			"fid" : $scope.featureid
		};

		WebService.invokePOSTRequest('workflow/getActionSettings', params, function(data) {

			$scope.data.feature = angular.fromJson(data);

			head.load($scope.data.feature.cssFiles);

			ScriptLoader.loadScript(0, $scope.data.feature.settingsJsFiles.concat($scope.data.feature.jsFiles), function() {

				safeApply($scope, function() {
					$scope.viewtemplate = angular.copy($scope.data.feature);
					$scope.settingstemplate = $scope.data.feature.settings;
				});
			});
		}, function(data) {
			toaster.pop('error', "Uh oh", "Could not get feature settings");
		});
	};

	$scope.okclicked = function() {

		$scope.showPreview = false;

		safeApply($scope, function() {
			$scope.data.feature.params = $scope.data.ok();
			$scope.viewtemplate = angular.copy($scope.data.feature);
		});

		$timeout(function() {
			$scope.showPreview = true;
		}, 100);
	};

	$scope.loadFeatureSettings();
});