App.factory('FeatureSettings',

[ '$q', '$modal', 'ScriptLoader',

function($q, $modal, ScriptLoader) {

	var s = {};

	s.showSettings = function(fc, f) {

		var deferred = $q.defer();

		if (fc.settingsCssFiles) {
			head.load(fc.settingsCssFiles);
		}

		ScriptLoader.loadScript(0, fc.settingsJsFiles, function() {

			var featurecopy = angular.fromJson(angular.toJson(f));

			var settingsInstance = $modal.open({
				keyboard : false,
				backdrop : 'static',
				size : fc.settingsSize || 'lg',
				templateUrl : 'core/js/modules/featuresettings/settingstemplate.html',
				controller : 'FeatureSettingsController',
				resolve : {
					featuredetails : function() {
						return fc;
					},
					feature : function() {
						return featurecopy;
					},
					originalfeature : function() {
						return f;
					},
					template : function() {
						return fc.settings;
					}
				}
			});

			settingsInstance.result.then(function(params) {
				if (f.type == 'function') {
					f.isPreFunction = featurecopy.isPreFunction;
				}

				f.featureDisplayName = featurecopy.featureDisplayName;

				deferred.resolve(params);
			}, function() {
				deferred.reject();
			});
		});

		return deferred.promise;
	};

	return s;
} ]);

App.controller('FeatureSettingsController',

[ '$scope', '$modal', '$modalInstance', 'ScriptLoader', 'feature', 'template', 'featuredetails', 'originalfeature',

function($scope, $modal, $modalInstance, ScriptLoader, feature, template, featuredetails, originalfeature) {

	$scope.showpreview = false;
	$scope.template = template;
	$scope.featuredetails = featuredetails;

	$scope.originalfeature = originalfeature;

	$scope.data = {
		'feature' : feature,
		'previewparams' : {},
		'title' : 'Edit settings',
		'ok' : function() {
			return 'cancel';
		}
	};

	$scope.preview = [];

	$scope.cancel = function() {
		$modalInstance.dismiss();
	};

	$scope.okclicked = function() {
		var okreturn = $scope.data.ok();

		if (okreturn != 'cancel') {
			$modalInstance.close(okreturn);
		}
	};

	$scope.previewselected = function() {
		if (!featuredetails.jsFiles) {
			return;
		}

		if (featuredetails.cssFiles) {
			head.load(featuredetails.cssFiles);
		}

		ScriptLoader.loadScript(0, featuredetails.jsFiles, function() {
			$scope.$apply(function() {
				$scope.preview.length = 0
				$scope.showpreview = false;

				$scope.preview.push({
					'view' : featuredetails.view,
					'id' : featuredetails.featureIncrementedId,
					'params' : $scope.data.previewparams
				});
				$scope.showpreview = true;
			});
		});
	};

} ]);