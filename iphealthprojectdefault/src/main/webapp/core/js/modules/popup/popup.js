App.controller('PopupController',

[ '$scope', '$http', '$modalInstance', 'title', 'message', 'type',

function($scope, $http, $modalInstance, title, message, type) {

	$scope.result = '';
	$scope.type = type;
	$scope.title = title;
	$scope.message = message;

	$scope.validateType = function() {
		if ((type !== 'ok') && (type !== 'close') && (type !== 'yesno') && (type !== 'okcancel')) {
			$scope.type = 'ok';
		}
	};

	$scope.dismiss = function() {
		$modalInstance.close($scope.result);
	};

	$scope.yes = function() {
		$scope.result = 'yes';
		$scope.dismiss();
	};

	$scope.no = function() {
		$scope.result = 'no';
		$scope.dismiss();
	};

	$scope.ok = function() {
		$scope.result = 'ok';
		$scope.dismiss();
	};

	$scope.cancel = function() {
		$scope.result = 'cancel';
		$scope.dismiss();
	};

	$scope.close = function() {
		$scope.result = 'close';
		$scope.dismiss();
	};

	$scope.validateType();
} ]);

App.factory('Popup',

[ '$rootScope', '$modal',

function($rootScope, $modal) {

	var s = {};

	s.show = function(title, message, type, size) {

		size = !size ? 'sm' : size == 'large' ? 'lg' : size == 'medium' ? undefined : size == 'small' ? 'sm' : undefined;

		return $modal.open({
			size : size,
			keyboard : false,
			backdrop : 'static',
			templateUrl : 'core/js/modules/popup/popup.html',
			controller : 'PopupController',
			resolve : {
				title : function() {
					return title;
				},
				message : function() {
					return message;
				},
				type : function() {
					return type;
				}
			}
		}).result;
	};

	return s;
} ]);