App.factory('PageBackground',

[ '$rootScope',

function($rootScope) {

	var s = {};

	s.images = [ {
		name : 'dallasdivide',
		url : 'https://az29176.vo.msecnd.net/videocontent/DallasDivideCO_TandemStock_66492190-768_EN-AU1395348791.jpg'
	}, {
		name : 'mangroves',
		url : 'https://www.bing.com/az/hprichbg/rb/SeedlingMangroves_EN-AU7545573826_1366x768.jpg'
	}, {
		name : 'slidingspringobservatory',
		url : 'https://www.bing.com/az/hprichbg/rb/SidingSpringObservatory_EN-AU11291728316_1366x768.jpg'
	}, {
		name : 'italybarcolana',
		url : 'https://www.bing.com/az/hprichbg/rb/ItalyBarcolana_EN-US12566281633_1366x768.jpg'
	}, {
		name : 'japanhitachinaka',
		url : 'https://www.istartedsomething.com/bingimages/cache/JapanHitachinaka_EN-US9012830558_1366x768.jpg'
	} ];

	s.change = function(im) {
		s.current = im;
		$rootScope.$broadcast('pagebackgroundchanged', s.current.url);
	}

	s.change(s.images[0]);

	return s;
} ]);

App.directive('pagebackground',

[ '$rootScope', '$window', 'PageBackground',

function($rootScope, $window, PageBackground) {

	return {
		restrict : 'A',
		scope : true,
		link : function(scope, element, attrs) {

			return; //Disabled until transparency added back in
			
			var maxPadding = 50.0;
			var windowEl = angular.element($window);

			scope.handler = function(e) {
				var scrolled = windowEl[0].scrollY;

				var max = element.height() - windowEl[0].innerHeight;
				var percent = scrolled / max;

				var padding = percent * maxPadding
				padding = padding > maxPadding ? maxPadding : padding;

				element.css({
					'background-position-y' : '-' + padding + 'px',
				});
			};

			scope.backChanged = function(url) {
				element.css({
					'background-image' : 'url(' + url + ')',
				});
			};

			$rootScope.$on('pagebackgroundchanged', function(evt, url) {
				scope.backChanged(url);
			});

			scope.backChanged(PageBackground.current.url);

			// Uncomment for 'parallax'
			// windowEl.on('scroll', scope.handler);
			// scope.handler();
		}
	};
} ]);
