var RadialMenu = function() {
	return {
		mx : 0,
		my : 0,
		data : [],
		show : false,
		callback : null,
		getDisplayItemFunction : null,

		// Gets called when radial menu item clicked
		onRadialMenuClick : function(menuData, data, dir) {
			if (menuData.callback) {
				menuData.callback(data, dir);
			}
		},

		reset : function() {
			this.data = [];
			this.show = false;
			this.callback = null;
		},

		xFunction : function() {
			var s = this;
			return function(d) {
				return s.getDisplayItemFunction ? s.getDisplayItemFunction(d) : d.id;
			};
		},

		yFunction : function() {
			return function(d) {
				return 1;
			};
		},

		toolTipContentFunction : function() {
			return function(key, x, y, e, graph) {
				return "";
			}
		},

		getMenuStyle : function(menuData) {

			if (menuData.show == true) {
				return {
					width : '200px',
					height : '200px',
					'z-index' : '500',
					position : 'absolute',
					top : menuData.mx + 'px',
					left : menuData.my + 'px',
				}
			} else {
				return {
					display : 'none'
				}
			}
		}
	}
};

App.directive('radialmenu',

[ '$timeout',

function($timeout) {

	var dir = {
		restrict : 'AE',
		scope : {
			menuinstance : '=',
		},
		templateUrl : 'core/js/modules/radialmenu/radialmenu.html',
		link : function(scope, element, attrs) {

			scope.$watch('menuinstance.data', function() {
				$timeout(reinitialize, 300);
			}, true);

			var clicked = function(index) {
				var data = index.currentTarget.__data__.data;
				scope.menuinstance.onRadialMenuClick(scope.menuinstance, data, dir);
			};

			var reinitialize = function() {
				$('#wfradialmenu g .nv-slice').each(function(index) {
					$(this).css('cursor', 'pointer');
					$(this).unbind('click', clicked);
					$(this).bind('click', clicked);
				});
			};

			$(window).load(function() {
				reinitialize();
			});
		}
	};

	return dir;
} ]);