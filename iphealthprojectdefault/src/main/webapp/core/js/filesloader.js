var FilesLoader = function(loadTrunkCoreFiles) {

	// --------------- CSS START -----------------

	this.bootstrapstyles = [

	// Animate css
	"_production/animate.css/animate.css",

	// Ionic icons
	"_production/ionicons/css/ionicons.min.css",

	// Bootstrap
	"_production/bootstrap/css/bootstrap.min.css",

	// Font awesome
	"_production/font-awesome/css/font-awesome.min.css",

	];

	this.trunkcorestyles = [ '_production/css/core.min.css' ];
	this.trunkmodulestyles = [ '_production/css/modules.min.css' ];
	this.uadminstyles = [ '_production/css/uadmin.min.css', "_production/css/modules.nonmini.min.css" ];

	// --------------- CSS END -----------------

	// --------------- JS START -----------------

	this.jquery = [ '_production/js/alljquery.min.js' ];
	this.angularjs = [ '_production/js/allangular.min.js' ];
	this.trunkcorescripts = [ '_production/js/core.min.js' ];
	this.trunkmodulescripts = [ '_production/js/modules.min.js' ];

	// --------------- JS END -----------------

	this.loadAllCss = function() {
		var s = this;
		head.load([].concat(s.bootstrapstyles, s.uadminstyles, s.trunkmodulestyles, s.trunkcorestyles));
	}

	this.loadAllJs = function() {
		var s = this;

		head.load(s.jquery, function() {
			head.load(s.angularjs, function() {
				head.load(s.trunkmodulescripts, function() {
					head.load(s.trunkcorescripts, function() {
					});
				});
			});
		});
	}
};