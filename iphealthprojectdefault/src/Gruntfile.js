module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		rootFolder : 'main/webapp',
		coreJsFolder : '<%= rootFolder %>/core/js',
		coreCssFolder : '<%= rootFolder %>/core/css',

		// angularJsFiles
		concat : {
			options : {
				separator : ';',
			},

			jqueryJs : {
				src : '<%= jqueryJsFiles %>',
				dest : '<%= rootFolder %>/_production/js/alljquery.min.js',
			},

			angularJs : {
				src : '<%= angularJsFiles %>',
				dest : '<%= rootFolder %>/_production/js/allangular.min.js',
			},

			coreJs : {
				src : '<%= coreJsFiles %>',
				dest : '<%= rootFolder %>/_production/js/core.min.js',
			},

			modulesJs : {
				src : '<%= modulesJsFiles %>',
				dest : '<%= rootFolder %>/_production/js/modules.min.js',
			},

			coreCss : {
				src : '<%= coreCssFiles %>',
				dest : '<%= rootFolder %>/_production/css/core.min.css',
			},

			modulesCss : {
				src : '<%= moduleCssFiles %>',
				dest : '<%= rootFolder %>/_production/css/modules.min.css',
			},

			moduleCssNonMinifyFiles : {
				src : '<%= moduleCssNonMinifyFiles %>',
				dest : '<%= rootFolder %>/_production/css/modules.nonmini.min.css',
			},

			uadminCssFiles : {
				src : '<%= uadminCssFiles %>',
				dest : '<%= rootFolder %>/_production/css/uadmin.min.css',
			},
		},

		uglify : {
			coreJs : {
				src : '<%= rootFolder %>/_production/js/core.min.js',
				dest : '<%= rootFolder %>/_production/js/core.min.js'
			},

			angularJs : {
				src : '<%= rootFolder %>/_production/js/allangular.min.js',
				dest : '<%= rootFolder %>/_production/js/allangular.min.js',
			},

			// Remove if errors start to pop up
			modulesJs : {
				src : '<%= rootFolder %>/_production/js/modules.min.js',
				dest : '<%= rootFolder %>/_production/js/modules.min.js'
			}
		},

		cssmin : {
			modulesCss : {
				src : '<%= rootFolder %>/_production/css/modules.min.css',
				dest : '<%= rootFolder %>/_production/css/modules.min.css'
			},

			coreCss : {
				src : '<%= rootFolder %>/_production/css/core.min.css',
				dest : '<%= rootFolder %>/_production/css/core.min.css'
			},

			uadminCssFiles : {
				src : '<%= rootFolder %>/_production/css/uadmin.min.css',
				dest : '<%= rootFolder %>/_production/css/uadmin.min.css',
			},
		},

		watch : {
			scripts : {
				files : '<%= allFiles %>',
				tasks : [ 'concat', 'uglify' ],
				options : {
					spawn : false,
				},
			}
		}

	});

	grunt.config.set('uadminCssFiles', [

	"<%= rootFolder %>/core/lib/uadmin/css/style.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/intro.js/introjs.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/calendar/zabuto_calendar.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/sco.message/sco.message.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/iCheck/skins/all.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jquery-notific8/jquery.notific8.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.1.1.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/summernote/summernote.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/ion.rangeSlider/css/ion.rangeSlider.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/nouislider/jquery.nouislider.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jquery-nestable/nestable.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jquery-toastr/toastr.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jstree/dist/themes/default/style.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jquery-treetable/stylesheets/jquery.treetable.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jquery-treetable/stylesheets/jquery.treetable.theme.custom.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-datepicker/css/datepicker.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/fullcalendar/fullcalendar.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/fullcalendar/fullcalendar.print.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/lightbox/css/lightbox.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/DataTables/media/css/jquery.dataTables.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/DataTables/media/css/dataTables.bootstrap.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jquery-tablesorter/themes/blue/style-custom.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/DataTables/media/css/dataTables.bootstrap.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-datepicker/css/datepicker3.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/dropzone/css/dropzone.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jquery-steps/css/jquery.steps.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-colorpicker/css/colorpicker.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-clockface/css/clockface.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-switch/css/bootstrap-switch.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/jplist/html/css/jplist-custom.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/select2/select2-madmin.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/bootstrap-select/bootstrap-select.min.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/multi-select/css/multi-select-madmin.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/x-editable/select2/lib/select2-madmin.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/x-editable/bootstrap3-editable/css/bootstrap-editable.css",

	"<%= rootFolder %>/core/lib/uadmin/vendors/x-editable/inputs-ext/address/address.css",

	"<%= rootFolder %>/core/lib/ui-select/css/select.min.css"

	]);

	grunt.config.set('moduleCssFiles', [

	"<%= rootFolder %>/core/lib/loader/css/loading-bar.css",

	"<%= rootFolder %>/core/lib/xeditable/css/xeditable.css",

	"<%= rootFolder %>/core/lib/toastr/css/toaster.css",

	"<%= rootFolder %>/core/lib/ng-tags-input/css/ng-tags-input.min.css",

	"<%= rootFolder %>/core/lib/ng-tags-input/css/ng-tags-input.min.css",

	"<%= rootFolder %>/core/lib/ng-table/css/ng-table.min.css",

	"<%= rootFolder %>/core/lib/nvd-3/css/nv.d3.min.css",

	"<%= rootFolder %>/core/lib/ng-tags-input/css/ng-tags-input.bootstrap.css",

	"<%= rootFolder %>/core/lib/datetimepicker/css/datetimepicker.css"

	]);

	grunt.config.set('moduleCssNonMinifyFiles', [

	"<%= rootFolder %>/core/lib/hint-tooltip/hint.min.css"

	]);

	grunt.config.set('coreCssFiles', [

	"<%= rootFolder %>/core/lib/uadmin/css/themes/style1/blue-grey.css",

	"<%= rootFolder %>/core/lib/uadmin/css/style-responsive.css",

	"<%= rootFolder %>/core/css/animations.css",

	"<%= rootFolder %>/core/css/custom.css",

	"<%= rootFolder %>/core/css/donttouch.css", ]);

	grunt.config.set('allCssFiles', [

	'<%= moduleCssFiles %>',

	'<%= coreCssFiles %>',

	'<%= uadminCssFiles %>',

	'<%= moduleCssNonMinifyFiles %>'

	]);

	grunt.config.set('jqueryJsFiles', [

	"<%= rootFolder %>/core/lib/jquery/jquery.min.js",

	"<%= rootFolder %>/core/lib/bootstrap/js/bootstrap.min.js",

	"<%= rootFolder %>/core/lib/jquery/jquery-ui.min.js"

	]);

	grunt.config.set('angularJsFiles', [

	"<%= rootFolder %>/core/lib/angular/angular.js",

	"<%= rootFolder %>/core/lib/angular/angular-route.js",

	"<%= rootFolder %>/core/lib/angular/angular-animate.js",

	"<%= rootFolder %>/core/lib/angular/angular-sanitize.js",

	"<%= rootFolder %>/core/lib/angular/angular-cookies.js"

	]);

	grunt.config.set('modulesJsFiles', [

	// http://blog.stevenlevithan.com/archives/date-time-format
	"<%= rootFolder %>/core/lib/dateformat/dateformat.js",

	"<%= rootFolder %>/core/lib/momentjs/moment.js",

	// Jmpress
	"<%= rootFolder %>/core/lib/jmpress/js/jmpress.js",

	// Toaster
	"<%= rootFolder %>/core/lib/toastr/js/toaster.js",

	"<%= rootFolder %>/core/lib/angular-bootstrap/ui/ui-bootstrap.js",

	"<%= rootFolder %>/core/lib/uadmin/vendors/slimScroll/jquery.slimscroll.js",

	"<%= rootFolder %>/core/lib/loader/js/loading-bar.js",

	"<%= rootFolder %>/core/lib/xeditable/js/xeditable.js",

	"<%= rootFolder %>/core/lib/ng-table/js/ng-table.js",

	"<%= rootFolder %>/core/lib/ng-tags-input/js/ng-tags-input.min.js",

	"<%= rootFolder %>/core/lib/ui-sortable/ui-sortable.js",

	// http://dalelotts.github.io/angular-bootstrap-datetimepicker/
	"<%= rootFolder %>/core/lib/datetimepicker/js/datetimepicker.js",

	// Ment.io
	"<%= rootFolder %>/core/lib/ment-io/js/mentio.js",

	"<%= rootFolder %>/core/lib/ment-io/js/templates.js",

	// ND3 charts
	"<%= rootFolder %>/core/lib/d3/js/d3.min.js",

	"<%= rootFolder %>/core/lib/nvd-3/js/nv.d3.min.js",

	"<%= rootFolder %>/core/lib/nvd-3/js/angularjs-nvd3-directives.min.js",

	// https://github.com/ninjatronic/angular-base64
	"<%= rootFolder %>/core/lib/angular-base64/lib/angular-base64.min.js",

	// Textangular
	"<%= rootFolder %>/core/lib/textangular/js/textAngular-sanitize.min.js",

	"<%= rootFolder %>/core/lib/textangular/js/textAngular.min.js",

	// UI-Select
	"<%= rootFolder %>/core/lib/ui-select/js/select.min.js",

	// ScriptJS
	"<%= rootFolder %>/core/lib/scriptjs/scriptjs.js",

	// http://blog.getelementsbyidea.com/load-a-module-on-demand-with-angularjs/
	// | https://github.com/ocombe/ocLazyLoad
	"<%= rootFolder %>/core/lib/oclazyload/ocLazyLoad.js"

	]);

	grunt.config.set('coreJsFiles', [

	'<%= coreJsFolder %>/**/**/**/**/*.js',

	'!<%= coreJsFolder %>/filesloader.js'

	]);

	grunt.config.set('allJsFiles', [

	'<%= modulesJsFiles %>',

	'<%= coreJsFiles %>',

	]);

	grunt.config.set('allFiles', [

	'<%= allCssFiles %>',

	'<%= allJsFiles %>'

	]);

	grunt.loadNpmTasks('grunt-css');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', [ 'concat', 'uglify', 'cssmin', 'watch' ]);
};